<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;

use App\Http\Controllers\rumahsakit\PermohonanKalibrasiController;

use App\Http\Controllers\admin\Admincontroller;

// tambahan master 
use App\Http\Controllers\AlatController;
use App\Http\Controllers\ProvinsiController;
use App\Http\Controllers\KotaController;
use App\Http\Controllers\KategorialatController;
// use App\Http\Controllers\HargaalatController;

Route::get('/', function () {
    return redirect()->route('home');
});


//permision kontroller  admin
Route::group(['middleware' => ['role:admin']], function() {
   //master
   // Route::get('/',[App\Http\Controllers\admin\Admincontroller::class, 'index'])->name('admin.page');
   Route::get('admin-page',[Admincontroller::class, 'index'])->name('admin.page');

   Route::get('admin-validasi',[Admincontroller::class, 'validasi'])->name('admin.validasi');
   Route::put('admin-validasi/{id}',[Admincontroller::class, 'validasi_admin'])->name('validasi_admin.validasi');
   Route::get('datas-bon',[Admincontroller::class, 'datas'])->name('bon.datas');
   //role
   Route::get('admin-roleuser',[App\Http\Controllers\admin\Admincontroller::class, 'admin_roleuser'])->name('role.admin_userrole');
   Route::get('admin-user',[App\Http\Controllers\admin\Admincontroller::class, 'admin_user'])->name('role.admin_user');
   // json
   Route::get('admin-user/{id}',[App\Http\Controllers\admin\Admincontroller::class, 'admin_user_id'])->name('role.admin_user_id');
   Route::post('admin-user/assign',[App\Http\Controllers\admin\Admincontroller::class, 'admin_user_assign'])->name('role.admin_user_assign');


   Route::resource('pegawai',App\Http\Controllers\admin\Pegawaicontroller::class);
   Route::resource('divisi',App\Http\Controllers\admin\Divisicontroller::class);
   Route::resource('barang',App\Http\Controllers\admin\Barangcontroller::class);
   Route::resource('jenis',App\Http\Controllers\admin\Jeniscontroller::class);
   Route::resource('pemasok',App\Http\Controllers\admin\PemasokController::class);
   Route::resource('satuan',App\Http\Controllers\admin\SatuanController::class);
   // transaksi barang masuk 
   Route::get('transaksi-masuk',[App\Http\Controllers\admin\Transaksimasukcontroller::class,'index'])->name('transaksimasuk');
   Route::post('transaksi-masuk',[App\Http\Controllers\admin\Transaksimasukcontroller::class,'store'])->name('transaksimasuk.store');
   Route::get('view-transaksi-masuk',[App\Http\Controllers\admin\Transaksimasukcontroller::class,'viewtransaksimasuk'])->name('transaksimasuk.view');
   //transaksi barang keluar
   Route::get('admin-transaksi-keluar',[App\Http\Controllers\admin\Transaksikeluarcontroller::class,'index'])->name('admin.transaksikeluar');
   //Laporan masuk
   Route::get('laporan-masuk',[App\Http\Controllers\admin\Laporanmasukcontroller::class,'index'])->name('laporanmasuk');
   Route::get('laporan-filtercari',[App\Http\Controllers\admin\Laporanmasukcontroller::class,'filtercari'])->name('laporanfiltercari');
   Route::post('laporan-masuk',[App\Http\Controllers\admin\Laporanmasukcontroller::class,'laporanmasukpost'])->name('laporanmasukpost');
  
   //Laporan keluar
   Route::get('laporan-keluar',[App\Http\Controllers\admin\Laporankeluarcontroller::class,'index'])->name('laporankeluar');
   Route::post('laporan-keluar',[App\Http\Controllers\admin\Laporankeluarcontroller::class,'laporanpost'])->name('laporanpost');

   //backup
   Route::get('admin-backup',[App\Http\Controllers\Backupcontrollermanual::class,'index'])->name('admin.backup');
   Route::get('admin-backup-mysql',[App\Http\Controllers\Backupcontrollermanual::class,'our_backup_database'])->name('our_backup_database');
});

//permision kontroller user
Route::group(['middleware' => ['role:user']], function() {
      // Route::get('/',[App\Http\Controllers\admin\Admincontroller::class, 'index'])->name('user.page');
   Route::get('user-page',[App\Http\Controllers\user\Usercontroller::class, 'index'])->name('user.page');

   // riwayat permohonan pegawai
   Route::get('permohonan-pegawai',[App\Http\Controllers\user\Usercontroller::class, 'getpermohonanpegawai'])->name('riwayat.permohonanpegawai');
   // end pp
   Route::get('pegawai-data',[App\Http\Controllers\user\Usercontroller::class, 'pegawaiData'])->name('pegawai.data');
   Route::post('pegawai-save',[App\Http\Controllers\user\Usercontroller::class, 'savePegawai'])->name('pegawai.save');

   Route::get('user-permohonan-kalibrasi',[App\Http\Controllers\user\Usercontroller::class, 'userKalibrasiRS'])->name('user.permohonankalrs');
   Route::get('user-permohonandl/{id}',[App\Http\Controllers\user\Usercontroller::class, 'cekpermohonandl'])->name('cekpermohonandl.get');
   Route::post('post-permohonan-kalibrasi',[App\Http\Controllers\user\Usercontroller::class, 'userkalibrasistore'])->name('user.userkalibrasistore');
   

   //pesan barang
   Route::get('user-pesan-barang',[App\Http\Controllers\user\UserPesanbarangcontroller::class, 'index'])->name('user.pesan');
   Route::post('simpan-pesan-barang',[App\Http\Controllers\user\UserPesanbarangcontroller::class, 'store'])->name('user.simpanbarang');
   Route::get('lihat-pesan-barang',[App\Http\Controllers\user\UserPesanbarangcontroller::class, 'lihatorder'])->name('user.lihatbarang');
   Route::get('lihat-pesan-barang/{id}',[App\Http\Controllers\user\UserPesanbarangcontroller::class, 'lihatdetailorder'])->name('user.lihatbarangdetail');
   
   // riwayat permohonan petugas jadwal 
   Route::get('/rumahsakit/riwayatpetugas',[PermohonanKalibrasiController::class,'riwayatpetugas'])->name('riwayatpetugas.index');

   // generate surat tugas 
   Route::post('generate-st',[PermohonanKalibrasiController::class,'cetakST'])->name('cetakST.index');
   
   // validasi
   Route::put('validasi/{id}',[App\Http\Controllers\user\UserPesanbarangcontroller::class,'validasi'])->name('user.validasi');
   //get status
   Route::get('get-status/{id}',[App\Http\Controllers\user\UserPesanbarangcontroller::class,'statusbon'])->name('user.statusbon');
   //laporan barang
   Route::get('laporan-barang-user',[App\Http\Controllers\user\UserPesanbarangcontroller::class, 'index'])->name('laporanbarang.user');
//approvement
});


Route::group(['middleware' => ['rumahsakit']], function () {
   // Rute-rute rumahsakit di sini
   Route::get('/rumahsakit', [App\Http\Controllers\rumahsakit\Rumahsakitcontroller::class, 'index'])->name('rumahsakit');
   Route::resource('/rumahsakit/permohonan',App\Http\Controllers\rumahsakit\PermohonanKalibrasiController::class);
   // Route::resource('/rumahsakit/transaksi-kalibrasi-rumahsakit',App\Http\Controllers\rumahsakit\TransaksiKalibrasiRumahsakitController::class);

   Route::get('/rumahsakit/data', [PermohonanKalibrasiController::class, 'getData'])->name('PermohonanKalibrasiController.data');
   Route::get('/rumahsakit/detail', [PermohonanKalibrasiController::class, 'getDetail'])->name('PermohonanKalibrasiController.detail');
   Route::get('/get-harga/{alat_id}', [PermohonanKalibrasiController::class, 'getHarga']);

   Route::get('/rumahsakit/riwayat',[PermohonanKalibrasiController::class,'riwayat'])->name('riwayat.index');

});
Auth::routes();

/*
Route::get('/rumahsakit', [App\Http\Controllers\rumahsakit\Rumahsakitcontroller::class, 'index'])->name('rumahsakit');
Route::resource('/rumahsakit/permohonan',App\Http\Controllers\rumahsakit\PermohonanKalibrasiController::class);
// Route::resource('/rumahsakit/transaksi-kalibrasi-rumahsakit',App\Http\Controllers\rumahsakit\TransaksiKalibrasiRumahsakitController::class);

Route::get('/rumahsakit/data', [PermohonanKalibrasiController::class, 'getData'])->name('PermohonanKalibrasiController.data');
Route::get('/rumahsakit/detail', [PermohonanKalibrasiController::class, 'getDetail'])->name('PermohonanKalibrasiController.detail');
Route::get('/get-harga/{alat_id}', [PermohonanKalibrasiController::class, 'getHarga']);
*/

// test
Route::get('/home', [HomeController::class, 'index'])->name('home');


// Route::group(['middleware' => ['auth']], function() {
//     Route::resource('roles', RoleController::class);
//     Route::resource('users', UserController::class);
//     Route::resource('products', ProductController::class);
// });

Route::resource('alats', AlatController::class);
Route::resource('provinsis', ProvinsiController::class);
Route::resource('kotas', KotaController::class);
Route::resource('kategorialats', KategorialatController::class);
// Route::resource('hargaalats', HargaalatController::class);



