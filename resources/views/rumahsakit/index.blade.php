<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <title>Rumahsakit - Dashboard</title>
      <link rel="shortcut icon" href="assets/img/favicon.png">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;0,900;1,400;1,500;1,700&display=swap" rel="stylesheet">
      @include('rumahsakit.layouts.css')
      <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        tr:hover {
            background-color: #f2f2f2;
        }
        th {
            background-color: #f2f2f2;
        }
        @media screen and (max-width: 600px) {
            th, td {
                display: block;
                width: 100%;
            }
        }
    </style>
   </head>
   <body>
      <div class="main-wrapper">
         @include('rumahsakit.layouts.header')
         @include('rumahsakit.layouts.sidebar')
         <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Dashboard</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                           <li class="breadcrumb-item active"></li>
                        </ul>
                     </div>
                  </div>
               </div>
               {{-- konten here --}}
               <div class="row">
                    <div class="col-xl-6 d-flex">
                        <div class="card flex-fill comman-shadow">
                            <div class="card-header d-flex align-items-center">
                            <h5 class="card-title ">Alat Terkalibrasi</h5>
                            <ul class="chart-list-out student-ellips">
                                <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a>
                                </li>
                            </ul>
                            </div>
                            <div class="card-body">
                            <div class="activity-groups" >
                                <div class="activity-awards">
                                    <p>test</p>
                                </div> 
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 d-flex">
                        <div class="card flex-fill comman-shadow">
                            <div class="card-header d-flex align-items-center">
                            <h5 class="card-title ">Monitoring Permohonan</h5>
                            <ul class="chart-list-out student-ellips">
                                <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a>
                                </li>
                            </ul>
                            </div>
                            <div class="card-body">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nomer Transaksi</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1?>
                                        @foreach ($totallistpermohonan as $item)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$item->nomorsurat_rs}}</td>
                                            <td>

                                                @if($item->status == 1)
                                                    Proses
                                                @else
                                                    Selesai
                                                @endif

                                            
                                            </td>
                                        </tr>
                                        @endforeach
                                        <!-- Tambahkan baris lainnya di sini sesuai kebutuhan -->
                                    </tbody>
                                </table>


                            {{-- <div class="activity-groups" >
                                <div class="activity-awards">
                                    <p>Data Permohonan Kalibrasi</p>
                                </div> 
                            </div> --}}
                            </div>
                        </div>
                    </div>


                    <!-- <div class="col-xl-4 d-flex">
                        <div class="card flex-fill comman-shadow">
                            <div class="card-header d-flex align-items-center">
                            <h5 class="card-title ">Alat Terkalibrasi</h5>
                            <ul class="chart-list-out student-ellips">
                                <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a>
                                </li>
                            </ul>
                            </div>
                            <div class="card-body">
                            <div class="activity-groups" >
                                <div class="activity-awards">
                                    <p>test</p>
                                </div> 
                            </div>
                            </div>
                        </div>
                    </div> -->
                </div>
               {{-- end konten --}}
            </div>
         </div>
      </div>
      @include('rumahsakit.layouts.js');
   </body>
</html>