<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">
                    <span>Main Menu</span>
                </li>
                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Menu</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{URL::to('rumahsakit/permohonan')}}">Permohonan Kalibrasi</a></li>
                        {{-- <li><a href="#">Transaksi Kalibrasi</a></li> --}}
                        <li><a href="{{URL::to('rumahsakit/riwayat')}}">Riwayat</a></li>
                        <li><a href="#">Utilitas</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
