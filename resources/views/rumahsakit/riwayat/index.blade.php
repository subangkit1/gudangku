@extends('rumahsakit.apprumahsakit')
@section('konten')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p style="font-size: 2em">Halaman Riwayat</p>
                <table class="table" id="myTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomer Transaksi</th>
                            <th>Nomer Surat</th>
                            <th>Status</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $i=1;
                            ?>
                        @foreach($permohonankalibrasi as $data)
                            <tr>
                               <td>{{$i++}}</td>
                               <td>TRX/{{$data->id}}/2024</td>
                               <td>{{$data->nomorsurat_rs}}</td>
                               <td>{{$data->status == 0 ? "Belum Diterima": "Sudah Diterima"}}</td>
                                    
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>








@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
// table = new DataTable('#myTable');
   @if(session()->has('success'))
   toastr.success('{{ session('success') }}', 'BERHASIL!');
   @elseif(session()->has('error'))
     toastr.error('{{ session('error') }}', 'GAGAL!');
   @endif
</script>
@endsection