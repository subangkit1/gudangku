@extends('rumahsakit.apprumahsakit')
@section('konten')

<div class="row">
   
    <div class="col-md-4">
       <div class="card">
          <div class="card-body">
            <h5 style="background-color: #2d3436; color: #dfe6e9; padding: 10px; border-radius: 10px;">Form Permohonan</h5>
          <form action="#" method="POST">
              @csrf
              <div class="form-group">
                  <label for="namaPemohon">Nomer Surat</label>
                  <input type="text" class="form-control" id="nomorsurat" name="namaPemohon" placeholder="Masukkan Nama Pemohon" required>
              </div>
              {{-- <div class="form-group">
                  <label for="emailPemohon">Perihal</label>
                  <input type="email" class="form-control" id="perihal" name="emailPemohon" placeholder="Masukkan Email Pemohon" required>
              </div> --}}
              {{-- <div class="form-group">
                  <label for="tanggalPermohonan">Tanggal Permohonan</label>
                  <input type="date" class="form-control" id="tanggalPermohonan" name="tanggalPermohonan" required>
              </div> --}}
          </form>
          </div>
       </div>
    </div>



    <div class="col-md-8">
        <div class="card">
            <div class="card-body">  
            <textarea name="editor1">
            &NonBreakingSpace; &NonBreakingSpace; Dengan ini kami sampaikan permohonan untuk kalibrasi alat-alat {{$user->name}} dengan rincian berikut :
            </textarea>
            <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
            <script>
                CKEDITOR.replace( 'editor1' );
            </script>

            </div>
        </div>
    </div>
</div>


<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">

            {{-- open --}}
               <table id="formTable" class="table table-bordered">
                   <thead>
                       <tr>
                           <th>Nama Alat</th>
                           <th>Jumlah</th>
                           <th>Harga Satuan</th>
                           <th>Total</th>
                           <th><button type="button" id="addRow" class="btn btn-sm btn-success">Tambah</button></th>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>
                               <select class="form-control namaAlat">
                                       <option value="0">Pilih Alat</option>
                                       @foreach($alat as $item)
                                       <option value="{{ $item->id }}">{{ $item->nama_alat }}</option>
                                       @endforeach
                               </select>
                           </td>
                           <td><input type="number" class="form-control qty" min="1" value="1"></td>
                           <td><input type="number" class="form-control hargaSatuan" readonly></td>
                           <td><input type="number" class="form-control total" readonly></td>
                           <td><button type="button" class="btn btn-sm btn-danger deleteRow">Hapus</button></td>
                       </tr>
                   </tbody>
               </table>
           
               <h6>Total Harga Semua: <span id="totalSemua">0</span></h6>
           

            {{-- close --}}
           
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <div class="text-right" style="float: right;">
                <button type="button" id="simpanBtn" class="btn btn-sm btn-success"> <i class="fas fa-save"></i> Simpan</button>
            </div>
         </div>
      </div>
   </div>
</div>
 




@endsection
@section('js')
{{-- <script src="https://localhost:8000/assets/js/jquery-3.6.0.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
{{-- <link rel="stylesheet" href="sweetalert2.min.css"> --}}
<script>
   $(document).ready(function(){
    //  Simpan data barang
    $('#simpanBtn').click(function(){
        //declare variabel input 

        var csrfToken = $('meta[name="csrf-token"]').attr('content');

        var nomorsurat = $('#nomorsurat').val();
        var perihal = $('#perihal').val();
        var p1 = $('[name="editor1"]').val();

        var requestHeader = {
            'nomorsurat' : nomorsurat,
            'perihal' : perihal,
            'paragraph' : p1
        }

        var permohonanAlat= [];
        $('#formTable tbody tr').each(function(){
            var namaAlat = $(this).find('.namaAlat').val();
            var qty = $(this).find('.qty').val();
            var hargaSatuan = $(this).find('.hargaSatuan').val();
        
        permohonanAlat.push({
            nama_alat: namaAlat,
            qty: qty,
            harga: hargaSatuan
            });
        });
        console.log(permohonanAlat);
        //masukkan dengan array push

        //kirim ke controllernya 
        $.ajax({
        url: "{{ route('permohonan.store') }}",
        type: 'POST',
        data: {alat: permohonanAlat, _token: csrfToken, requestHeader: requestHeader},
        success: function(response){
            console.log(response) // Display success message
           
            const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.onmouseenter = Swal.stopTimer;
                toast.onmouseleave = Swal.resumeTimer;
            }
            });
            Toast.fire({
            icon: "success",
            title: "Simpan in successfully"
            });
            setTimeout(function() {
                window.location.href = "{{route('permohonan.index')}}"; // Redirect to the permohonan index page after a delay
            }, 3000); // Wait for 3 seconds before redirecting
            // Here you can do whatever you want after the data is saved
            },
            error: function(xhr, status, error){
            console.error(error);
            alert('An error occurred while saving the equipment data');
            }
        });


        // end ajac


    });

     // Fungsi untuk menambah row
     $("#addRow").click(function(){
       var newRow = '<tr><td><select class="form-control namaAlat"><option value="0">Pilih Alat</option>@foreach($alat as $item)<option value="{{ $item->id }}">{{ $item->nama_alat }}</option>@endforeach</select></td><td><input type="number" class="form-control qty" min="1" value="1"></td><td><input type="number" class="form-control hargaSatuan" readonly></td><td><input type="number" class="form-control total" readonly></td><td><button type="button" class="btn btn-danger btn-sm deleteRow">Hapus</button></td></tr>';
       $("#formTable").append(newRow);
     });


   
     // Fungsi untuk menghapus row
     $(document).on('click', '.deleteRow', function(){
       $(this).closest('tr').remove();
       hitungTotalSemua();
     });
   
     // Fungsi untuk menghitung total harga setiap baris
     $(document).on('input', '.qty', function(){
       var qty = $(this).val();
       var hargaSatuan = $(this).closest('tr').find('.hargaSatuan').val();
       var total = qty * hargaSatuan;
       $(this).closest('tr').find('.total').val(total);
       hitungTotalSemua();
     });
   
     // Fungsi untuk menghitung total harga semua
     function hitungTotalSemua(){
       var totalSemua = 0;
       $('.total').each(function(){
         totalSemua += parseInt($(this).val());
       });
       totalSemua = totalSemua.toLocaleString('id-ID', { style: 'currency', currency: 'IDR' });
       $("#totalSemua").text(totalSemua);
     }
   
     // Fungsi untuk menampilkan harga satuan berdasarkan nama alat yang dipilih
     $(document).on('change', '.namaAlat', function(){
       var alat_id = $(this).val();
       var row = $(this).closest('tr');
       $.ajax({
           url: '/get-harga/' + alat_id,
           method: 'GET',
           success: function(response){
               row.find('.hargaSatuan').val(response.harga);
               var qty = row.find('.qty').val();
               var total = qty * response.harga;
               row.find('.total').val(total);
               hitungTotalSemua();
           }
       });
     });
   });
   </script>

@endsection