<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Tugas</title>
    <style>
        body {
            font-family: "Times New Roman", Times, serif;
        }
        /* styling lainnya */
        /* Pastikan untuk menyesuaikan styling sesuai dengan kebutuhan */

        table {
            /* width: 100%; */
            border-collapse: collapse;
        }
        td {
            /* text-align: right; */
            padding: 5px;
        }

        .header {
            text-align: center;
            margin-bottom: 20px;
            /* line-height: 1.5;  */

        }
        .header h1 {
            margin: 0;
            font-size: 18px
        }
        .header p {
            margin: 5px 0;
        }
        p {
        line-height: 1.2; /* Sesuaikan dengan nilai yang diinginkan */
         }

         hr {
        border: none; /* Menghapus garis bawaan */
        border-top: 2px solid black; /* Membuat garis horizontal dengan ketebalan 2px */
        /* font-weight: bold; Membuat garis horizontal menjadi tebal */
        }
    </style>
</head>
<body>
    {{-- <header>
        <img src="url_gambar_kop" alt="Kop Surat">
        
    </header> --}}

    <div class="header">
        <p><h1>Balai Pengamanan Alat dan Fasilitas Kesehatan (BPAFK) Surabaya</h1><p>
        <p>Alamat: Jl. Karang Menjangan No.22, Airlangga, Kec. Gubeng, Surabaya, Jawa Timur 60286</p>
        <p>Telepon: (031) 5035830</p>
    </div>
    <hr>

    <br><br>
    <table align="left">
        <tr>
            <td style="width: 100px;">Nomor</td>
            <td>: - </td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>: - </td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>: Surat Tugas</td>
        </tr>
    </table>
    <br><br>
    <br>
    <br><br>
    <br>
    <div>
        <p>Kepada Yth.</p>
        <p>Pimpinan Rumah {{@$rumahsakit->nama_rs}}</p>
        <p>{{@$rumahsakit->alamat_rs}}</p>
        <p>di tempat</p>
    </div>
    <br>
    <br>
    <br>
    <div>
        <p>Dengan Hormat,</p>
        <p>Bersama ini kami sampaikan surat tugas yang dari Balai Pengamanan Alat Faslitas Kesehatan Surabaya, dengan petugas yang akan ke Faskes sebagai berikut:</p>
        <table border="1" cellspacing="0" cellpadding="5" style="width: 100%;">
            <tr>
                <th>No</th>
                <th>Nama Pegawai</th>
                <th>NIK</th>
                <th>Jabatan</th>
            </tr>
            <?php $i = 1;?>
            @foreach ($map as $data)    
            <tr>
                <td>{{ $i++}}</td>
                <td>{{$data->pegawai->nama}}</td>
                <td>{{$data->pegawai->nip}}</td>
                <td>{{$data->pegawai->jabatan}}</td>   
            </tr>
            @endforeach
        </table>
        <p>Demikianlah surat tugas ini kami sampaikan. Terima kasih atas perhatian dan kerjasamanya.</p>
        <br>
        
        <table align="right">
            <tr>
                <td colspan="2">an Kepala Balai,</td>
            </tr>
            <tr>
                <td colspan="2">Kepala Pelayanan Teknis Kalibrasi</td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="https://www.emoderationskills.com/wp-content/uploads/2010/08/QR1.jpg" alt="Barcode Dummy" style="max-width: 100px;">
                </td>
            </tr>
            <tr>
                <td colspa="2">RIKYAN HERNAWATI, S.Si, M.Kes</td>
                
            </tr>
            <tr>
                <td>NIP.197102041998032002</td>
            </tr>
        </table>

        
    </div>
</body>
</html>
