@extends('user.appuser')
@section('konten')
<div class="row">
   @if(auth()->user()->id == 3)
   <div class="col-xl-4 col-sm-4 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
            <div class="db-widgets d-flex justify-content-between align-items-center">
               <div class="db-info">
                  <h6>Jumlah Transaksi Saat ini</h6>
                  <h3>
                     {{$bon}}                     
                  </h3>
               </div>
               <div class="db-icon">
                  <img src="{{ asset('icon_svg/action-blocks-svgrepo-com.svg') }}" width="40" alt="Dashboard Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   @endif

   @if(auth()->user()->id == 3)
   <div class="col-xl-4 col-sm-4 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
            <div class="db-widgets d-flex justify-content-between align-items-center">
               <div class="db-info">
                  <h6>Transaksi Belum Setuju</h6>
                  <h2><a href="{{route('user.lihatbarang')}}" style="color:black">{{$bonnoacc  ?  $bonnoacc : '0'}}</a></h2>
               </div>
               <div class="db-icon">
                  <img src="{{ asset('icon_svg/attestationdedeplacement-svgrepo-com.svg') }}" width="40" alt="Dashboard Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   

   <div class="col-xl-4 col-sm-4 col-12 d-flex">
      <div class="card bg-light w-100">
         <div class="card-body">
            <div class="db-widgets d-flex justify-content-between align-items-center">
               <div class="db-info">
                  <h6>Transaksi Setuju</h6>
                  <h2>{{$bonacc ?  $bonacc : '0'}}</h2>
               </div>
               <div class="db-icon">
                  <img src="{{ asset('icon_svg/attestationdedeplacement-svgrepo-com.svg') }}" width="40" alt="Dashboard Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endif


<div class="row">
    <div class="col-xl-12 col-sm-12 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
             @if (Auth::check()==true)
                @if (Auth::user()->id == '3')
            {{-- table bu rikyan--}}
            <canvas id="myChart" width="200" height="100"></canvas>
             @endif
                @endif

            {{-- table tabel bu rikyan --}}

            

            {{-- user --}}
            @if (Auth::check()==true)
                @if (Auth::user()->id != '3')

            
            <div class="row">
               <div class="col-md-12">
                  {{-- <div class="card"> --}}
                     {{-- <div class="card-body"> --}}
                        <span class="infoproses">
                        

                        </span>
                     {{-- </div> --}}
                  {{-- </div> --}}
               </div>
            </div>
            
            {{-- table user biasa --}}
            <div class="row">
               <div class="col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <div class="table-responsive">

                        <table class="table">
                           <thead>
                              <tr>
                                 <th scope="col">No</th>
                                 <th scope="col">OrderId</th>
                                 <th scope="col">NomerSurat / Keterangan</th>
                                 <th scope="col">Barang</th>
                                 <th></th>
                              </tr>
                           </thead>
                       <tbody>
                           <?php $i=1 ?>
                           @foreach ($bon as $items)
                           <tr>
                              <td scope="row">{{$i++}}</td>
                              <td>{{$items->id_pesanan}}</td>
                              <td>{{$items->keterangan}}</td>
                              <td width=auto>
                                 @foreach($items->bondetail as $barang)
                                 {{$barang->tobarang->nama_barang}} -  <span class="badge bg-info text-light">{{$barang->jumlah}} pcs</span><br>
                                 @endforeach
                              </td>
                              <td><button class="btn btn-sm btn-light" onclick="getinfo({{$items->id_pesanan}})"><i class="fa fa-info-circle"></i> INFO</button></td>
                           </tr>
                           @endforeach
                         
                       </tbody>
                     
                       </table>
                       </div>

                     </div>
                  </div>
               </div>
            </div>

            {{-- test data --}}

            
            {{-- end test --}}
            

            {{-- end user --}}

            {{-- detail dari bon --}}
            
            
            
            {{-- end table user biasa --}}
            @endif
                @endif
            
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script type="text/javascript">
   $(document).ready(function(){
   $('#myTable').dataTable();
   })
   // table = new DataTable('#myTable');
      @if(session()->has('success'))
      toastr.success('{{ session('success') }}', 'BERHASIL!');
      @elseif(session()->has('error'))
        toastr.error('{{ session('error') }}', 'GAGAL!');
      @endif
</script>

<script>
var ctx = document.getElementById("myChart").getContext('2d');
var labelss = {!!($nama)!!};
var jumlahs = {{($jumlah)}};
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!!($nama)!!},
        datasets: [{
            label: '#Stok Barang',
            data: {{($jumlah)}},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
   function getinfo(id)
   {
      let token = $("meta[name='csrf-token']").attr("content");
      $.ajax({
         url:`get-status/${id}`,
         type:"GET",
         cache:false,
         data:{
            "id":id,
            "_token":token
         },
         success:function(data){
            $(".infoproses").html("")
            var html = '';
            var data = data[0];
            console.log(data);

            var step_satu = 0;
            var step_dua = 1;
            var step_tiga = 2;
            var step_empat = 3;

            var status = data.status;
            var enable = '#00b894';
            var disable = '#dfe6e9';

            html = '<div class="row">';

            html += '<div class="col-md-3">';
               if(step_satu <= status){
                  html += '<div class="card" style="background-color:'+enable+';color:aliceblue;">';
                  }else{
                  html += '<div class="card" style="background-color:'+disable+'">';
                  }
                     html += '<div class="card-body">';
                        html += '<div class="card-title">Proses ke-1 <i class="fe fe-box"></i>';
                        html += '</div>';
                        html += '<div class="card-text">Order barang :  '+data.keterangan;
                        html += '</div>';
                     html += '</div>';
                  html += '</div>';
               html += '</div>';


               html += '<div class="col-md-3">';
                 if(step_dua <= status){ 
                  html += '<div class="card" style="background-color:'+enable+';color:aliceblue;">';
                   }else{
                     html += '<div class="card" style="background-color:'+disable+'">';
                     }
                    html += ' <div class="card-body">';
                        html += '<div class="card-title">Proses ke-2 <i class="fe fe-box"></i>';
                        html += '</div>';
                        html += '<div class="card-text">Order sudah disetujui Bu Rikyan';
                        html += '</div>';
                     html += '</div>';
                  html += '</div>';
               html += '</div>';

               html +='<div class="col-md-3">';
                  if(step_tiga <= status){ 
                  html += '<div class="card" style="background-color:'+enable+';color:aliceblue;">';
                  }else{
                     html += '<div class="card" style="background-color:'+disable+'">';
                  }
                    html +=' <div class="card-body">';
                       html +=' <div class="card-title">Proses ke-3 <i class="fe fe-box"></i>';
                       html +=' </div>';
                       html +=' <div class="card-text">Order diproses oleh petugas';
                       html +=' </div>';
                    html +=' </div>';
                 html +=' </div>';
               html +='</div>';

               html +='<div class="col-md-3">';
               if(step_tiga <= status){ 
                 html += '<div class="card" style="background-color:'+enable+';color:aliceblue;">';
                  }else{
                     html += '<div class="card" style="background-color:'+disable+'">';
                  }
                    html +=' <div class="card-body">';
                      html +='  <div class="card-title">Proses ke-4 <i class="fe fe-thumbs-up"></i>';
                      html +='  </div>';
                      html +='  <div class="card-text">Barang boleh diambil';
                      html +='  </div>';
                    html +=' </div>';
                 html +=' </div>';
             html +='  </div>';

             html += '</div>';

             $(".infoproses").append(html)

         }
      });
   }

</script>

@endsection