@extends('user.appuser')
@section('konten')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <p>Daftar Petugas yang berangkat : </p>

                    <table class="table" id="myTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rumah Sakit</th>
                                    <th>Tgl Berangkat</th>
                                    <th>Tgl Pulang</th>
                                    <th>Nama Pegawai</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1 ?>
                            @foreach($permohonan as $dt)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{@$dt->getRumahsakit->nama_rs}}</td>
                                        <td>{{@$dt->getpermohonanpegawai[0]->tanggal}}
                                            {{-- {{dd($dt->getpermohonanpegawai[0])}} --}}
                                            {{-- {{dd(count($dt->getpermohonanpegawai) - 1)}} --}}
                                        </td>
                                        {{-- mengambil data terakhir key atau array untuk diambil tanggal --}}
                                        <?php 
                                        $arraytotal = count($dt->getpermohonanpegawai);
                                        $dipakai = $arraytotal - 1;
                                        ?>
                                        {{-- {{dd($dt->getpermohonanpegawai[$dipakai]->tanggal)}} --}}
                                        <td>{{@$dt->getpermohonanpegawai[$dipakai]->tanggal}}</td>
                                        <td>
                                            @php
                                                $uniqueNames = [];
                                            @endphp
                                            <?php $countpegawai =1 ?>
                                            @foreach ($dt->getpermohonanpegawai as $datapp)
                                            {{-- {{dd($datapp)}} --}}
                                            {{-- array uniqe agar tidak sama saat di cetak --}}
                                            @php
                                            
                                            $nama = $datapp->getpegawai->nama;
                                            if (!in_array($nama, $uniqueNames)) {
                                                $uniqueNames[] = $nama;
                                            }
                                            @endphp
                                           
                                            {{-- end array  --}}
                                            {{-- <ul>
                                                <li>{{$countpegawai++}}. {{$datapp->getpegawai->nama}}</li>
                                            <ul> --}}
                                            @endforeach
                                            <?php $countd = 1 ?>
                                            @foreach ($uniqueNames as $nama)
                                            <ul>
                                                <li>{{$countd++}}.{{ $nama }}</li>
                                            </ul>
                                            @endforeach


                                            
                                        </td>
                                    </tr>
                            @endforeach 
                            </tbody>
                        </table>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<link href="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script type="text/javascript">
</script>

<script>
    $(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
    
</script>
@endsection
