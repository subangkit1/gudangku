<div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
               <div id="sidebar-menu" class="sidebar-menu">
                  <ul>
                     <li class="menu-title">
                        <span>Main Menu</span>
                     </li>

                     @if (auth::check()==true)
                        @if (auth::user()->id == '3')
                        <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span> Permohonan </span> <span class="menu-arrow"></span></a>
                        <ul>
                           <li><a href="{{URL::to('/user-permohonan-kalibrasi')}}">Permohonan rumahsakit</a></li>
                           <li><a href="{{URL::to('/permohonan-pegawai')}}">Riwayat Petugas Kalibrasi</a></li>
                        </ul>
                        </li>   
                        @endif
                        
                     @endif


                     <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span>Jadwal</span> <span class="menu-arrow"></span></a>
                        <ul>
                           
                           @if(auth::check()==true)
                              @if (auth::user()->id == '3')
                              <li><a href="#">Jadwal Kalibrasi Semua Petugas</a></li>
                              @else
                              <li><a href="{{URL::to('/rumahsakit/riwayatpetugas')}}">Jadwal Kalibrasi</a></li>
                              @endif
                           @endif
                          
                           
                        </ul>
                     </li>

                     <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span>Pemesanan</span> <span class="menu-arrow"></span></a>
                        <ul>
                           <li><a href="{{ route('user.pesan') }}">Pesan barang</a></li>
                           @if(auth::check()==true)
                              @if (auth::user()->id == '3')
                              <li><a href="{{URL::to('/lihat-pesan-barang')}}">Data Pesan Barang</a></li>
                              @else
                              <li><a href="{{URL::to('/user-page')}}">Data Pesan Barang</a></li>
                              @endif
                           @endif
                          
                           
                        </ul>
                     </li>

                     @if (auth::check()==true)
                        @if (auth::user()->id == '3')
                        <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span> Validasi</span> <span class="menu-arrow"></span></a>
                        <ul>
                           <li><a href="{{URL::to('/lihat-pesan-barang')}}">Approvment</a></li>
                        </ul>
                        </li>   
                        @endif
                        
                     @endif

                     
                     @if (auth::check()==true)
                        @if (auth::user()->id == '3')
                        <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span> Permohonan kalibrasi</span> <span class="menu-arrow"></span></a>
                        <ul>
                           <li><a href="{{URL::to('/lihat-pesan-barang')}}">Kotak Masuk</a></li>
                        </ul>
                        </li>   
                        @endif
                        
                     @endif

                      <li class="submenu">
                        <a href="#"><i class="fe fe-grid"></i> <span> Laporan</span> <span class="menu-arrow"></span></a>
                        <ul>
                           <li><a href="{{ route('laporanmasuk') }}">Laporan Pemesanan Barang</a></li> 
                        </ul>
                     </li>

                  </ul>
               </div>
            </div>
         </div>