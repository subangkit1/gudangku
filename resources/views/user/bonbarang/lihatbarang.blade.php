@extends('user.appuser')
@section('konten')
<div class="row">
   <div class="col-xl-12 col-sm-12 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
            @if (Auth::check()==true)
            @if (Auth::user()->id == '3')
            {{-- table bu rikyan--}}
            <div class="table-responsive">
               <table class="table table-striped mb-0" id="myTable">
                  <thead class="table-dark">
                     <tr>
                        <th>No</th>
                        <th>Pegawai</th>
                        <th>Divisi</th>
                        <th>Tanggal </th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $no=1 ;?>
                     @foreach($data as $item)
                     <tr>
                        <td>{{$no++}}</td>
                        <td>{{$item->pegawai->name}}</td>
                        <td>{{@$item->pegawai->divisi->nama_divisi !== null ? $item->pegawai->divisi->nama_divisi : '-' }}</td>
                        <td>{{\Carbon\Carbon::parse($item->tanggal_bon)->format("Y-m-d")}}</td>
                        <td>{{$item->keterangan}}</td>
                        @if($item->status == '0')
                        <?php $status = '<b class="badge bg-danger text-dark">Belum di Validasi</b>'; ?>
                        @else
                        <?php $status = '<b class="badge bg-success text-white">Tervalidasi</b>'; ?>
                        @endif
                        <td>{!!$status!!}</td>
                        <td><button class="btn btn-default btn-sm" style="color:white" onclick="bonDetail({{$item->id_pesanan}})"><img width="20" height="20" src="https://img.icons8.com/ios-filled/50/fine-print.png" alt="fine-print"/></button>
                           @if($item->status == '0')
                           <button class="btn btn-warning btn-sm" style="color:black" onclick="validasi({{$item->id_pesanan}})">Validasi</button>
                           @else
                           @endif
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            @endif
            @endif
            {{-- table tabel bu rikyan --}}
            @if (Auth::check()==true)
            @if (Auth::user()->id != '3')
            {{-- table user biasa --}}
            <p>tabel user</p>
            {{-- end table user biasa --}}
            @endif
            @endif
         </div>
      </div>
   </div>
</div>
{{-- untuk modal --}}
<div class="row">
   <div class="col-md-12">
      
            {{-- modal --}}
            <!-- Modal -->
            <div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Barang</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                     </div>
                     <div class="modal-body">

                        <span id="datadetail">

                        </span>
                        
                     </div>
                     <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button> --}}
                     </div>
                  </div>
               </div>
            </div>
            
            {{-- end modal --}}
        
      
   </div>
</div>
{{-- end modal --}}
@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
   // alert("test");
   $('#myTable').dataTable();
   })
   // table = new DataTable('#myTable');
      @if(session()->has('success'))
      toastr.success('{{ session('success') }}', 'BERHASIL!');
      @elseif(session()->has('error'))
        toastr.error('{{ session('error') }}', 'GAGAL!');
      @endif
   
    function validasi(id){
        let token = $("meta[name='csrf-token']").attr("content");
        let id_header = id;
        $.ajax({
            url :`/validasi/${id}`,
            type:"PUT",
            cache:false,
            data:{
                "id":id,
                "_token":token
            },
            success:function(data){
                console.log(data)
                window.location.replace("http://localhost:8000/lihat-pesan-barang");
            }
   
        });
   
        // alert(token);
    }
   
    function bonDetail(id){
        $("#datadetail").html("");
        let token = $("meta[name='csrf-token']").attr("content");
        let id_header = id;
        $.ajax({
            url :`/lihat-pesan-barang/${id}`,
            type:"GET",
            cache:false,
            data:{
                "id":id,
                "_token":token
            },
            success:function(data){
                console.log(data[0]['jumlah'])
                var number = 1;
                var length = data.length
                console.log(length)
                var html = '<div class="table table-responsive">';
                           html += '<table class="table table-responsive table-striped">';
                              html += '<thead class="table-dark">';
                                 html += '<th>No</th>';
                                 html += '<th>Barang</th>';
                                 html += '<th>Jumlah</th>';
                                 html += '<th>Satuan</th>';
                              html += '</thead>';
                              html += '<tbody>';
                                for(var i=0;i<length;i++){
                                    html += '<tr>';
                                    html += '<td>'+(number+i)+'</td>';
                                    html += '<td>'+data[i]['namabarang']['nama_barang']+'</td>';
                                    html += '<td>'+data[i]['jumlah']+'</td>';
                                    html += '<td>Pcs</td>';
                                 html +='</tr>';
                                } 
                              html += '</tbody>';
                           html += '</table>';
                        html += '</div>';
                // modal show
                $("#datadetail").append(html);
                $("#exampleModal").modal('show')
            }
   
        });
    }
</script>
@endsection