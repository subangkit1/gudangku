@extends('user.appuser')
@section('konten')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <H6 class="btn btn-primary btn-sm">JADWAL KALIBRASI</H6>
                    <p class="card-text">Berikut ini jadwal yang sudah disetujui oleh Pelayanan : </p>
                    @if(empty($mapquery))
                    <div class="jumbotron">
                        <center><h4 class="display-6">Belum ada Jadwal</h4></center>
                      </div>
                    @else
                    <div class="table-responsive" >
                        <table class="table" id="myTable">
                          <thead>
                            <tr>
                                <th>No</th>
                              <th>No Surat</th>
                              <th>Nama Faskes</th>
                              <th>Tanggal Berangkat</th>
                              <th>Tanggal Pulang</th>
                              <th>Generate ST</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i =1;?>
                            @foreach ($mapquery as $item)
                                
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ @$item->permohonan->nomorsurat_rs }}</td>
                                <td>{{@$item->rs->nama_rs}}</td>
                                <td>
                                    <?php 
                                        $counter = count($item->tanggalkalibrasi);
                                        $counter_akhir = $counter - 1;
                                    ?>
                                    {{@$item->tanggalkalibrasi[0]->tanggal}} 
                                </td>
                                <td>
                                    {{-- cek tanggal array nya jika o maka tampilkan o jika tidak ambil array terakhir --}}
                                    @if($counter<1)
                                        {{@$item->tanggalkalibrasi[0]->tanggal}}
                                    @else
                                        {{@$item->tanggalkalibrasi[$counter_akhir]->tanggal}}
                                    @endif
                                </td>
                                <td>
                                    <form action="{{route('cetakST.index')}}" method="POST" id="myForm{{ @$item->permohonan_id }}" target="_blank">
                                        @csrf <!-- Tambahkan ini untuk melindungi formulir dari serangan CSRF -->
                                        <input type="hidden" name="permohonan_id" value="{{ $item->permohonan_id }}">
                                        <input type="hidden" name="pegawai_ids" value="{{ $item->pegawai_ids }}">
                                        <button type="submit" class="btn btn-sm btn-primary submitBtn">Generate</button>
                                    </form>

                                </td>
                            </tr>
                            @endforeach
                            <!-- Tambahkan baris-baris tabel sesuai dengan kebutuhan -->
                          </tbody>
                        </table>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')

<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<link href="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script type="text/javascript">
</script>

<script>
    $(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
    
</script>
@endsection
