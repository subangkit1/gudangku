@extends('user.appuser')
@section('konten')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>Total Permohonan saat ini : </p>
                {{-- modal --}}
                <div class="modal fade" id="pegawaidialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Form Pilih Petugas </h5>
                                <button type="button" id="closedialog" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <span class="test"></span>
                                {{-- open form --}}
                                <form action="#" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        {{-- <label for="status">Pegawai:</label> --}}

                                    <table id="pegawaiTable" class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th></th>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Position</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- Data will be loaded here dynamically using DataTables --}}
                                        </tbody>
                                    </table>

                                    
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="start_date">Start Date:</label>
                                        <input type="date" class="form-control" id="start_date" name="start_date[]" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="end_date">End Date:</label>
                                        <input type="date" class="form-control" id="end_date" name="end_date[]" required>
                                    </div>
                                    
                                </form>

                                {{-- end form --}}
                            </div>
                            <div class="modal-footer">
                                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                                <button type="button" class="btn btn-primary" id="saveChanges">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal --}}


                {{-- modal kedua --}}
                {{-- modal --}}
                <div class="modal fade" id="pegawaidialog_revisi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color:#2d3436;color:#dfe6e9">
                                <h5 class="modal-title" id="exampleModalLabel" style="color: #dfe6e9">Form Pilih Petugas Revisi</h5>
                                {{-- <button type="button" id="closedialog" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button> --}}
                            </div>
                            <div class="modal-body">
                                <form action="{{route('user.userkalibrasistore')}}" method="POST">  
                                <span class="test"></span>
                                {{-- open form --}}
                                
                                    @csrf
                                    <div class="form-group">
                                    
                                    </div>
                                    <div class="form-group">
                                        <label for="start_date">Start Date:</label>
                                        <input type="date" class="form-control" id="start_date" name="start_date" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="end_date">End Date:</label>
                                        <input type="date" class="form-control" id="end_date" name="end_date" required>
                                    </div>
                                    
                                    
                                    {{-- end form --}}
                                </div>
                                <div class="modal-footer">
                                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                                    <button type="submit" class="btn btn-primary" id="saveChanges">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- end modal --}}

                {{-- end modal kedua --}}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p><h6>Permohonan Kalibrasi Rumah Sakit</h6></p>

                {{-- table --}}
                <table class="table table-striped mb-0 display table-responsive" id="example" cellspacing="0">
                    <thead class="table-dark">
                       <tr>
                          <th>No</th>
                          <th>Nomer Surat</th>
                          <th>Rumah Sakit</th>
                          <th class="none">Alat</th>
                          <th>Prioritas</th>
                          <th>Tanggal Pemohon</th>
                          {{-- <th class ="none">Jarak Instansi</th> --}}
                          <th>Status</th>
                          <th></th>
                       </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nomer Surat</th>
                            <th>Rumah Sakit</th>
                            <th>Alat</th>
                            <th>Prioritas</th>
                            <th>Tanggal Pemohon</th>
                            {{-- <th>Jarak Instansi</th> --}}
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach ($permohonan as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$data->nomorsurat_rs}}</td>
                            <td>{{@$data->getRumahsakit->nama_rs}}</td>
                            <td>
                                @foreach (@$data->getDetail as $item)
                                <ol>
                                <li>{{@$item->getAlat->nama_alat}}</li>
                                </ol>
                                @endforeach
                            </td>
                            <td>
                                
                                <?php $total =  @$data->getPrioritas->prioritas_satu + @$data->getPrioritas->prioritas_dua ?>

                                @if($total > 100)
                                    <span class="badge badge-pill badge-danger"> {{$total}} </span>
                                    @if(@$data->getPrioritas->prioritas_tiga < 1)
                                        <a href="/setlokasi" style="color: black">  <i class="fas fa-plus-circle filled" style="color: black"></i> Set</a>
                                    @else
                                        <i class="fas fa-compass"> {{$data->getPrioritas->prioritas_tiga}}</i>
                                    @endif

                                @elseif($total < 100 && $total > 80)
                                    <span class="badge badge-pill badge-warning"> {{$total}} </span>
                                    @if($data->getPrioritas->prioritas_tiga < 1)
                                    <a href="/setlokasi" style="color: black">  <i class="fas fa-plus-circle filled" style="color: black"></i> Set</a>
                                    @else
                                        <i class="fas fa-compass"> {{$data->getPrioritas->prioritas_tiga}}</i>
                                    @endif
                                @else
                                

                                <span class="badge badge-pill badge-info"> {{$total}} </span>
                                @if(@$data->getPrioritas->prioritas_tiga < 1)
                                 <a href="/setlokasi" style="color: black">  <i class="fas fa-plus-circle filled" style="color: black"></i> Set</a>
                                @else
                                <i class="fas fa-compass"> {{$data->getPrioritas->prioritas_tiga}}</i>
                                 @endif
                                

                                @endif



                            </td>
                            <td>{{ \Carbon\Carbon::parse($data->tgl_insert)->translatedFormat('d F Y') }}</td>
                            <td>
                                @if($data->status == 0)
                                <span class="badge badge-pill badge-danger">Belum diproses</span>
                                @else
                                <span class="badge badge-success">Sudah diproses</span>
                                @endif
                            </td>
                            {{-- <td></td> --}}
                            <td>
                                <button class="btn btn-sm btn-info" style="color: white" onclick="permohonanDL({{$data->id}})"><i class="fa fa-arrow-alt-circle-right"></i> Pegawai revisi </button>

                               
                                
                                {{-- set disable jika sudah di proses --}}
                                @if($data->status == 0)
                                <button class="btn btn-sm btn-info" style="color: white" onclick="permohonanpegawai({{$data->id}})"><i class="fa fa-arrow-alt-circle-right"></i> Pegawai </button>
                                
                                @else
                                <button disabled class="btn btn-sm btn-default" style="color: grey" onclick="permohonanpegawai({{$data->id}})"><i class="fa fa-arrow-alt-circle-right"></i> Pegawai </button>
                                @endif
                                
                                
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>

                {{-- end table --}}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>Info Prioritas : <span class="badge badge-pill badge-danger"> 100 > High Priority </span> <span class="badge badge-pill badge-warning"> 100 < and 80 > Medium Priority </span>
                     <span class="badge badge-pill badge-info"> 80 < Low Priority </span></p>
                <p>Set : Setting Jarak Rumah sakit untuk prioritas tambahan mengetahui Jarak antara Kantor menuju Rumah Sakit / Faskes  </p>
            </div>
        </div>
    </div>
</div>








@endsection
@section('js')



<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

{{-- selec2 --}}
<link href="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="http://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- end select2 --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script type="text/javascript">


$(document).ready(function() {
    console.log("test");
});

function permohonanDL(permohonanid) {
    // alert(permohonanid);
    var html = '';
    $.ajax({
            url: 'user-permohonandl/' + permohonanid,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                $('.test').html('')
                console.log(data);
                var permohonan_idku = data[0].permohanan_id;
                // check
                const uniquePetugasIds = new Set();
                const distinctData = [];
                for (const item of data) {
                    if (!uniquePetugasIds.has(item.petugas_id)) {
                        uniquePetugasIds.add(item.petugas_id);
                        distinctData.push(item);
                    }
                }
               // console.log(distinctData);
                    html = '<table class="table table-striped table-sm table-responsive" id="example" width="100%">';
                        html += '<thead>';
                            html += '<tr>';
                            html += '<th scope="col">No</th>';
                            html += '<th scope="col">Nama Petugas</th>';
                            html += '<th scope="col">Pilih</th>';
                            
                            html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        html +='<input type="hidden" name="permohonan_id" value="'+permohonan_idku+'"/>';
                        var n =1;
                    for(var i=0;i<distinctData.length;i++){
                        html += '<tr><td>' + (k = i+n) + '</td><td>' + distinctData[i].namaPegawai[0].nama + '</td><td><input type="checkbox" name="pegawai_id[]" value="'+ distinctData[i].petugas_id +'"></td></tr>';
                        console.log(distinctData[i].petugas_id);
                    }
                        html += '</tbody>';
                    html += '</table>';
                    console.log(html);
                // endcheck
                $('.test').append(html);
               
                // var p = "test";

                // $('.test').append(p);


                $('#pegawaidialog_revisi').modal('show');
            },
            error: function(error) {
                toastr.error('eror data');
            }
        });

}

function permohonanpegawai(permohonanid) {
        // cek permohonan id dengan pegawai 

        
        console.log('ID:', permohonanid);
        $('#permohonan_id').remove();       
        $('.js-example-basic-multiple').select2();
        $('#pegawaidialog').modal('show');


        $('#pegawaidialog').find('form').append('<input type="hidden" name="permohonanid" id="permohonan_id" value="' + permohonanid + '">');

        

    }
   

    
$('#closedialog').on('click', function() {
    $('#pegawaidialog').modal('hide');
});
 


 var table = $('#example').DataTable({
        'responsive': true
    });

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
        // Expand row details
        table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });

    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Collapse row details
        table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
    });
 
</script>

<script>
    $(document).ready(function() {
        var table = $('#pegawaiTable').DataTable({
            'ajax': '{{ route("pegawai.data") }}',
            'columns': [
                { 
                    "data": "user_id",
                    "render": function(data, type, row) {
                        return `<input type="checkbox" name="pegawai_id[]" value="${row.user_id}">`;
                    }
                },
                { "data": "user_id" },
                { "data": "name" },
                { "data": "position" }
            ]
        });

        $('#pegawaiTable tbody').on('click', 'input[type="checkbox"]', function() {
            console.log('Checkbox for row', $(this).closest('tr').index(), 'is now', this.checked);
            // console.log();
        });

        $('#saveChanges').on('click', function() {
            var data = table.$('input[type="checkbox"]').serialize();
            data += '&_token=' + $('meta[name="csrf-token"]').attr('content');
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();
            data += '&start_date=' + startDate + '&end_date=' + endDate;
            var permohonanId = $('#permohonan_id').val();
            data += '&permohonan_id=' + permohonanId;

            console.log(data);

            
            $.ajax({
            
                url: '{{ route("pegawai.save") }}',
                type: 'POST',
                data: data,
                success: function(response) {
                    toastr.success('Data Tersimpan ');
                    location.reload();

                    console.log(response);
                },
                error: function(error) {
                    toastr.error('eror data');
                }
            });
        });
    });
</script>
@endsection