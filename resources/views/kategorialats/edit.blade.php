@extends('layouts.app')

@section('content')
    <h1>Edit Kategori Alat</h1>

    <form action="{{ route('kategorialats.update', $kategorialat->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="kodekategori">Kode Kategori</label>
            <input type="text" name="kodekategori" class="form-control" value="{{ $kategorialat->kodekategori }}">
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" class="form-control" value="{{ $kategorialat->nama }}">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control" value="{{ $kategorialat->status }}">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
