@extends('admin.appadmin')

@section('konten')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
    <h5>Daftar Kategori Alat</h5>

    <a href="{{ route('kategorialats.create') }}" class="btn btn-primary mb-2">Tambah Kategori Alat</a>

    <table class="table" id="myTable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Kode Kategori</th>
                <th>Nama</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($kategorialats as $kategorialat)
                <tr>
                    <td>{{ $kategorialat->id }}</td>
                    <td>{{ $kategorialat->kodekategori }}</td>
                    <td>{{ $kategorialat->nama }}</td>
                    <td>{{ $kategorialat->status }}</td>
                    <td>
                        <a href="{{ route('kategorialats.show', $kategorialat->id) }}" class="btn btn-info btn-sm">Lihat</a>
                        <a href="{{ route('kategorialats.edit', $kategorialat->id) }}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('kategorialats.destroy', $kategorialat->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus kategori alat ini?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
// table = new DataTable('#myTable');
   @if(session()->has('success'))
   toastr.success('{{ session('success') }}', 'BERHASIL!');
   @elseif(session()->has('error'))
     toastr.error('{{ session('error') }}', 'GAGAL!');
   @endif
</script>
@endsection

