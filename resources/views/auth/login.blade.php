<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Login Gudang</title>

    <link rel="shortcut icon" href="assets/img/favicon.png">

    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;0,900;1,400;1,500;1,700&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/feather/feather.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/icons/flags/flags.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>

<body>

    <div class="main-wrapper login-body">
        <div class="login-wrapper">
            <div class="container">
                <div class="loginbox">
                    <div class="login-left">
                        <img class="img-fluid" src="{{ asset('assets/img/warehouse-login.png') }}" alt="Logo"
                            width="350px">
                    </div>
                    <div class="login-right">
                        <div class="login-right-wrap">
                            <h2>Login Aplikasi Gudang</h2>
                            {{-- <p class="account-subtitle">Need an account? <a href="register.html">Sign Up</a></p>
<h2>Sign in</h2> --}}
                            <form action="{{ route('login') }}" method="POST">
                                <div class="form-group">
                                    @csrf
                                    <label> <span class="login-danger"></span>{{ __('Email Address') }}</label>
                                    <input class="form-control" type="email" id="email" name="email" required
                                        autocomplete="false" placeholder="">
                                    <span class="profile-views"><i class="fas fa-user-circle"></i></span>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label> <span class="login-danger"></span>{{ __('Password') }}</label>
                                    <input class="form-control pass-input" type="password" id="password"
                                        name="password">
                                    <span class="profile-views feather-eye toggle-password"></span>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block"
                                        type="submit">{{ __('Login') }}</button>
                                </div>
                            </form>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('assets/js/jquery-3.') }}6.0.min.js"></script>

    <script src="{{ asset('assets/plugins/boot') }}strap/js/bootstrap.bundle.min.js"></script>

    <script src="{{ asset('assets/js/feather.m') }}in.js"></script>

    <script src="{{ asset('assets/js/script.js') }}"></script>
</body>

</html>
