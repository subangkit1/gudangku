@extends('admin.appadmin')
@section('konten')
<div class="row">

<div class="row">
    <div class="col-xl-12 col-sm-12 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
             
            {{-- table user biasa --}}
            <div class="row">
               <div class="col-xl-12 col-sm-12 col-12 d-flex">
                  <div class="card bg-comman w-100">
                     <div class="card-body">

                        <table class="table table-responsive table-striped ">
                           <thead>
                         <tr>
                           <th scope="col">No</th>
                           <th scope="col">OrderId</th>
                           <th scope="col">NomerSurat / Keterangan</th>
                           <th scope="col">Barang</th>
                           <th></th>
                         </tr>
                       </thead>
                       <tbody>
                       
                       
                         <tr>
                           <th scope="row"></th>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td><button class="btn btn-sm btn-light" onclick="getinfo()"><i class="fa fa-info-circle"></i> INFO</button></td>
                         </tr>
                         
                         
                       </tbody>
                     
                       </table>

                     </div>
                  </div>
               </div>
               

            </div>

            {{-- detail dari bon --}}
            <div class="row">

               
            </div>
            
            
            {{-- end table user biasa --}}
           
            
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script type="text/javascript">
   $(document).ready(function(){
   $('#myTable').dataTable();
   })
   // table = new DataTable('#myTable');
      @if(session()->has('success'))
      toastr.success('{{ session('success') }}', 'BERHASIL!');
      @elseif(session()->has('error'))
        toastr.error('{{ session('error') }}', 'GAGAL!');
      @endif
</script>


<script>
   function getinfo(id)
   {
      alert(id)
   }

</script>

@endsection