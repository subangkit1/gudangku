<!-- resources/views/provinsis/create.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Tambah Provinsi Baru</h1>

    <form action="{{ route('provinsis.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama_provinsi">Nama Provinsi</label>
            <input type="text" name="nama_provinsi" class="form-control">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
