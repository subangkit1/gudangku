<!-- resources/views/provinsis/index.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Daftar Provinsi</h1>

    <a href="{{ route('provinsis.create') }}" class="btn btn-primary mb-2">Tambah Provinsi</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama Provinsi</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($provinsis as $provinsi)
                <tr>
                    <td>{{ $provinsi->id }}</td>
                    <td>{{ $provinsi->nama_provinsi }}</td>
                    <td>{{ $provinsi->status }}</td>
                    <td>
                        <a href="{{ route('provinsis.show', $provinsi->id) }}" class="btn btn-info btn-sm">Lihat</a>
                        <a href="{{ route('provinsis.edit', $provinsi->id) }}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('provinsis.destroy', $provinsi->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus provinsi ini?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
