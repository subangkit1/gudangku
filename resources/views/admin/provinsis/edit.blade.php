<!-- resources/views/provinsis/edit.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Edit Provinsi</h1>

    <form action="{{ route('provinsis.update', $provinsi->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_provinsi">Nama Provinsi</label>
            <input type="text" name="nama_provinsi" class="form-control" value="{{ $provinsi->nama_provinsi }}">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control" value="{{ $provinsi->status }}">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
