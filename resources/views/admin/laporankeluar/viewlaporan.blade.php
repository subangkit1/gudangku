<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>laporan barang keluar</title>
</head>

<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
    }
</style>

<body>

    <center>
        <h4 style="font-family: font-family: Arial, Helvetica, sans-serif;">Laporan Stok Barang Keluar</h4>
    </center>
    <table class="table" id="customers">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Petugas</th>
                <th>Nama Barang</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            @foreach ($data as $dt)
                <tr>
                    <td scope="row">{{ $i++ }}</td>
                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->nama_barang }}</td>
                    <td>{{ $dt->tanggal_bon }}</td>
                    <td>{{ $dt->jumlah }} /Pcs</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4">
                    Total Barang Keluar
                </td>
                <td>{{ $keluartotal[0]->total }} /Pcs</td>
            </tr>

        </tbody>
    </table>

</body>

</html>
