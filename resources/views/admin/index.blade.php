<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
      <title>Admin - Dasboard</title>
      <link rel="shortcut icon" href="assets/img/favicon.png">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;0,900;1,400;1,500;1,700&display=swap" rel="stylesheet">
      @include('admin.layouts.css')
   </head>
   <body>
      <div class="main-wrapper">
         @include('admin.layouts.header')
         @include('admin.layouts.sidebar')
         <div class="page-wrapper">
            <div class="content container-fluid">
               <div class="page-header">
                  <div class="row">
                     <div class="col">
                        <h3 class="page-title">Dashboard</h3>
                        <ul class="breadcrumb">
                           <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                           <li class="breadcrumb-item active"></li>
                        </ul>
                     </div>
                  </div>
               </div>
               {{-- konten here --}}
               <div class="row">
                  <div class="col-xl-6 d-flex">
                     <div class="card flex-fill student-space comman-shadow">
                        <div class="card-header d-flex align-items-center">
                           <h5 class="card-title">Barang</h5>
                           <ul class="chart-list-out student-ellips">
                              <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a>
                              </li>
                           </ul>
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">


                              <table class="table star-student table-hover table-center table-borderless table-striped">
                                 <thead class="thead-light">
                                    <tr>
                                       <th>No</th>
                                       <th>Nama Barang</th>
                                       <th class="text-center">Jenis Barang</th>
                                       <th class="text-center">Jumlah</th>
                                       <th class="text-end">Harga</th>
                                       {{-- <th class="text-end"></th> --}}
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $i=1?>
                                    @foreach ( $barang as $item)
                                    <tr>
                                       <td class="text-nowrap">
                                          <div>{{ $i++ }}</div>
                                       </td>
                                       <td class="text-nowrap">
                                          {{ $item->nama_barang }}
                                       </td>
                                       <td class="text-center">{{ @$item->jenisbarang[0]->nama_jenisbarang }}</td>
                                       <td class="text-center">

                                          @if($item->jumlah < 30)

                                          <span style="color:red">{{ $item->jumlah }}</span>

                                          @else
                                          {{ $item->jumlah }}
                                          @endif
                                          
                                          
                                       
                                       </td>
                                       <td class="text-end">
                                          <div>{{ $item->harga }}</div>
                                       </td>

                                       {{-- <td> <a href="" class="btn btn-sm btn-primary" style="color:white">status</a></td> --}}
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>

                              {!! $barang->withQueryString()->links('pagination::bootstrap-5') !!}
                              <p>* Stok dibawah : 30 berwarna <i style="color:red">Merah</i></p>

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-6 d-flex">
                     <div class="card flex-fill comman-shadow">
                        <div class="card-header d-flex align-items-center">
                           <h5 class="card-title ">Daftar Pemesanan Barang </h5>
                           <ul class="chart-list-out student-ellips">
                              <li class="star-menus"><a href="javascript:;"><i class="fas fa-ellipsis-v"></i></a>
                              </li>
                           </ul>
                        </div>
                        <div class="card-body">

                           @foreach($bonbarang as $items)
                           <div class="activity-groups">
                              <div class="activity-awards">
                                 <div class="award-boxs">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hospital" viewBox="0 0 16 16">
                                       <path d="M8.5 5.034v1.1l.953-.55.5.867L9 7l.953.55-.5.866-.953-.55v1.1h-1v-1.1l-.953.55-.5-.866L7 7l-.953-.55.5-.866.953.55v-1.1h1ZM13.25 9a.25.25 0 0 0-.25.25v.5c0 .138.112.25.25.25h.5a.25.25 0 0 0 .25-.25v-.5a.25.25 0 0 0-.25-.25h-.5ZM13 11.25a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5Zm.25 1.75a.25.25 0 0 0-.25.25v.5c0 .138.112.25.25.25h.5a.25.25 0 0 0 .25-.25v-.5a.25.25 0 0 0-.25-.25h-.5Zm-11-4a.25.25 0 0 0-.25.25v.5c0 .138.112.25.25.25h.5A.25.25 0 0 0 3 9.75v-.5A.25.25 0 0 0 2.75 9h-.5Zm0 2a.25.25 0 0 0-.25.25v.5c0 .138.112.25.25.25h.5a.25.25 0 0 0 .25-.25v-.5a.25.25 0 0 0-.25-.25h-.5ZM2 13.25a.25.25 0 0 1 .25-.25h.5a.25.25 0 0 1 .25.25v.5a.25.25 0 0 1-.25.25h-.5a.25.25 0 0 1-.25-.25v-.5Z"/>
                                       <path d="M5 1a1 1 0 0 1 1-1h4a1 1 0 0 1 1 1v1a1 1 0 0 1 1 1v4h3a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h3V3a1 1 0 0 1 1-1V1Zm2 14h2v-3H7v3Zm3 0h1V3H5v12h1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3Zm0-14H6v1h4V1Zm2 7v7h3V8h-3Zm-8 7V8H1v7h3Z"/>
                                     </svg>
                                 </div>
                                 <div class="award-list-outs">
                                    <h4>{{$items->keterangan ? $items->keterangan : '-'}}</h4>
                                    <h5>{{$items->pegawai->name}}

                                       @if($items->status == 0)
                                          <span class="badge rounded-pill bg-danger">Belum acc </span>
                                       @else
                                          <p >Sudah acc </p>
                                       @endif
                                    
                                    </h5>
                                 </div>
                                 <div class="award-time-list">
                                   <span>
                                    {{\Carbon\Carbon::now()->format('Y-m-d')}}
                                 </span>
                                 </div>
                              </div> 
                           </div>
                           @endforeach


                           {{-- Daftar bon belum di acc Selama ini --}}
                           

                           <div class="activity-groups" >
                              <div class="activity-awards">
                                 <div class="award-boxs" style="background-color:#ff7675">
                                    <img width="20" height="20" src="https://img.icons8.com/ios-filled/50/shopping-cart.png" alt="shopping-cart"/>
                                 </div>
                                 <div class="award-list-outs">
                                    <a href="#" style="color:black">Belum Approve</a>
                                    
                                 </div>
                                 <div class="award-time-list">
                                   

                                   <h6 style="color:#ff7675"> {{$notvalidateall ? $notvalidateall : '0'}}</h6>
                                    
                                 
                                 </div>
                              </div> 
                           </div>


                           {{-- end Daftar bon belum di acc --}}
                           

                           {{-- lihat semua --}}
                           <div class="activity-groups">
                              <div class="activity-awards">
                                 <div class="award-boxs" style="background-color: #81ecec">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right-square-fill" viewBox="0 0 16 16">
                                       <path d="M0 14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2v12zm4.5-6.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5a.5.5 0 0 1 0-1z"/>
                                     </svg>
                                 </div>
                                 <div class="award-list-outs">
                                    <a href="#" style="color:black">Lihat Semua</a>
                                    
                                 </div>
                                 <div class="award-time-list">
                                   
                                 </div>
                              </div> 
                           </div>
                           

                           {{-- end semua --}}
                        </div>
                     </div>
                  </div>
               </div>
               {{-- end konten --}}
            </div>
         </div>
      </div>
      @include('admin.layouts.js');
   </body>
</html>