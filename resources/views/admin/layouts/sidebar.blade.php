<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">
                    <span>Main Menu</span>
                </li>
                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Master</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        {{-- <li><a href="{{ route('pegawai.index') }}">Pegawai</a></li> --}}
                        <li><a href="{{ route('divisi.index') }}">Divisi</a></li>
                        <li><a href="{{ route('pemasok.index') }}">Pemasok</a></li>
                        <li><a href="{{ route('barang.index') }}">Barang</a></li>
                        <li><a href="{{ route('satuan.index') }}">Satuan</a></li>
                        <li><a href="{{ route('jenis.index') }}">Jenis</a></li>
                    </ul>
                </li>

                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Validasi</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{ route('admin.validasi') }}">Aprovement </a></li>
                    </ul>
                </li>

                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Transaksi</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{ route('transaksimasuk') }}">Barang Masuk</a></li>
                        <!-- <li><a href="{{ route('admin.transaksikeluar') }}">Barang Keluar</a></li> -->

                    </ul>
                </li>

                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Laporan</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{ route('laporanmasuk') }}">Laporan Barang Masuk</a></li>
                        <li><a href="{{ route('laporankeluar') }}">Laporan Barang Keluar</a></li>

                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="feather-grid"></i> <span> Utility</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{ route('role.admin_user') }}">User</a></li>
                        <li><a href="{{ route('role.admin_userrole') }}">Role</a></li>
                        <li><a href="{{ route('admin.backup') }}">Backup</a></li>

                    </ul>
                </li>


            </ul>
        </div>
    </div>
</div>
