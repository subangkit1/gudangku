<!-- resources/views/alats/create.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Tambah Alat Baru</h1>

    <form action="{{ route('alats.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama_alat">Nama Alat</label>
            <input type="text" name="nama_alat" class="form-control">
        </div>
        <div class="form-group">
            <label for="jam_alat">Jam Alat</label>
            <input type="text" name="jam_alat" class="form-control">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
