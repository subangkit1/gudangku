<!-- resources/views/alats/edit.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Edit Alat</h1>

    <form action="{{ route('alats.update', $alat->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_alat">Nama Alat</label>
            <input type="text" name="nama_alat" class="form-control" value="{{ $alat->nama_alat }}">
        </div>
        <div class="form-group">
            <label for="jam_alat">Jam Alat</label>
            <input type="text" name="jam_alat" class="form-control" value="{{ $alat->jam_alat }}">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control" value="{{ $alat->status }}">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
