@extends('layouts.app')

@section('content')
    <h1>Daftar Alat</h1>

    <a href="{{ route('alats.create') }}" class="btn btn-primary mb-2">Tambah Alat</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama Alat</th>
                <th>Jam Alat</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($alats as $alat)
                <tr>
                    <td>{{ $alat->id }}</td>
                    <td>{{ $alat->nama_alat }}</td>
                    <td>{{ $alat->jam_alat }}</td>
                    <td>{{ $alat->status }}</td>
                    <td>
                        <a href="{{ route('alats.show', $alat->id) }}" class="btn btn-info btn-sm">Lihat</a>
                        <a href="{{ route('alats.edit', $alat->id) }}" class="btn btn-primary btn-sm">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
