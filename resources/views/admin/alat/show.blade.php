<!-- resources/views/alats/show.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Detail Alat</h1>

    <p><strong>Nama Alat:</strong> {{ $alat->nama_alat }}</p>
    <p><strong>Jam Alat:</strong> {{ $alat->jam_alat }}</p>
    <p><strong>Status:</strong> {{ $alat->status }}</p>

    <a href="{{ route('alats.edit', $alat->id) }}" class="btn btn-primary">Edit</a>

    <form action="{{ route('alats.destroy', $alat->id) }}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Hapus</button>
    </form>
@endsection
