@extends('admin.appadmin')
@section('konten')
<div class="row">
   <div class="col-md-6">
      <div class="card" >
         <div class="card-body">
            <h5 class="card-title">Form Jenis Barang</h5>
            <form action="{{ route('jenis.store') }}" method="POST">
               {{ csrf_field() }}
               <input class="form-control form-control-sm mb-2" type="text" name="namajenis" placeholder="" aria-label=".form-control-sm example">
               <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
            </form>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            <table class="table caption-top" id="myTable">
               <caption>Tabel Jenis Barang</caption>
               <thead>
                  <tr>
                     <th scope="col">NO</th>
                     <th scope="col">NAMA JENIS BARANG</th>
                     <th scope="col"></th>
                  </tr>
               </thead>
               <tbody>
                <?php $i=1?>
                @foreach ($data as $item)
                  <tr>
                     <th scope="row">{{ $i++ }}</th>
                     <td>{{ $item->nama_jenisbarang }}</td>
                     <td>
                      <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('jenis.destroy', $item->id) }}" method="POST">
                                            <a href="{{ route('jenis.edit', $item->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                     </td>
                  </tr>
                @endforeach
                  
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
// table = new DataTable('#myTable');
   @if(session()->has('success'))
   toastr.success('{{ session('success') }}', 'BERHASIL!');
   @elseif(session()->has('error'))
     toastr.error('{{ session('error') }}', 'GAGAL!');
   @endif
</script>
@endsection