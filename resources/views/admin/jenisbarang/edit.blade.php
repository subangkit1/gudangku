@extends('admin.appadmin')
@section('konten')
<div class="row">
   <div class="col-md-6">
      <div class="card" >
         <div class="card-body">
            <h5 class="card-title">Form Jenis Barang Edit</h5>
            <form action="{{ route('jenis.update',$data->id) }}" method="POST">
               {{ csrf_field() }}
               @method('PUT')
               <input class="form-control form-control-sm mb-2" type="text" name="nama_jenisbarang" value="{{ $data->nama_jenisbarang }}" aria-label=".form-control-sm example">
               <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
            </form>
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@endsection