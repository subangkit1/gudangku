@extends('layouts.app')

@section('content')
    <h1>Daftar Kota</h1>

    <a href="{{ route('kotas.create') }}" class="btn btn-primary mb-2">Tambah Kota</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Provinsi</th>
                <th>Nama Kota</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($kotas as $kota)
                <tr>
                    <td>{{ $kota->id }}</td>
                    <td>{{ $kota->provinsi->nama_provinsi }}</td>
                    <td>{{ $kota->nama_kota }}</td>
                    <td>{{ $kota->status }}</td>
                    <td>
                        <a href="{{ route('kotas.show', $kota->id) }}" class="btn btn-info btn-sm">Lihat</a>
                        <a href="{{ route('kotas.edit', $kota->id) }}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('kotas.destroy', $kota->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus kota ini?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
