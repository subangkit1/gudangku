@extends('layouts.app')

@section('content')
    <h1>Detail Kota</h1>

    <p><strong>Provinsi:</strong> {{ @$kota->provinsi }}</p>
    <p><strong>Nama Kota:</strong> {{ $kota->nama_kota }}</p>
    <p><strong>Status:</strong> {{ $kota->status }}</p>

    <a href="{{ route('kotas.edit', $kota->id) }}" class="btn btn-primary">Edit</a>

    <form action="{{ route('kotas.destroy', $kota->id) }}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus kota ini?')">Hapus</button>
    </form>
@endsection
