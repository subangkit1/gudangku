@extends('admin.appadmin')
@section('konten')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('role.admin_userrole') }}" class="btn btn-sm btn-primary" style="margin-bottom: 10px">
                        Lihat Role</a>
                    {{-- table user --}}

                    <div class="table-responsive">
                        <table class="table table-striped" id="myTable_user">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email / Username </th>
                                    <th>Status </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($datauser as $user)
                                    <tr>
                                        <td>{{ $i++ }} </td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if ($user->role_id === null)
                                                <button class="btn btn-sm btn-warning"
                                                    onclick="assinRole({{ $user->id }})">
                                                    Masukkan Role</button>
                                            @else
                                                {{ $user->role_id === 1 ? 'Admin' : 'User' }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->role_id === null)
                                            @else
                                                <button class="btn btn-sm btn-primary">Update User</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- end user --}}
                </div>
            </div>
        </div>
    </div>
    {{-- end table --}}
    {{-- div untuk modal --}}
    <div class="row">
        <div class="col-md-12">
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Role</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('role.admin_user_assign') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group mb-0 row">
                                    <input type="hidden" name="model_id" id="model_id" />
                                    <label class="col-form-label col-md-4">Nama</label>
                                    <div class="col-md-6">
                                        <input type="text" name="name" id="nama"
                                            class="form-control form-control-sm" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-4">Level</label>
                                    <div class="col-md-8">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="role_id" value="1"> Admin
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="role_id" value="2"> User
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet"
        media="screen">
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    {{-- date --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="text/javascript">
        $(document).ready(function() {
            // select2
            $('#jenisbarang, #penyedia').select2({
                //update here
                tags: true,
                placeholder: " -Pilih- ",
                allowClear: true,
            });
            // alert("test");
            $('#datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
            });
            $('#myTable_user').dataTable();
        })
        // table = new DataTable('#myTable');
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
    <script type="text/javascript">
        function assinRole(id) {
            let model_id = id;
            $("#staticBackdrop").modal("show");
            // tapilkan ke view user rolenya 
            $.ajax({
                url: `admin-user/${model_id}`,
                type: "GET",
                cache: false,
                data: {
                    "id": id
                },
                success: function(data) {
                    console.log(data)
                    document.getElementById("nama").value = data[0].name;
                    document.getElementById("model_id").value = data[0].id;

                }

            });
        }
    </script>
@endsection
