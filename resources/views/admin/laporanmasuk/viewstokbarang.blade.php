<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>laporan Stok Barang Terupdate</title>
</head>

<style>
    #customers {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
    }
</style>

<body>

    <center>
        <h4 style="font-family: font-family: Arial, Helvetica, sans-serif;">Laporan Stok Barang Keluar</h4>
    </center>
    <table class="table" id="customers">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Jenis Barang</th>
                <th>Stok Masuk</th>
                <th>Stok Terupdate</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1;
            $stokmasuk = 0;
            $stokterkini = 0;
            ?>
            @foreach ($collect as $dt)
                <tr>
                    <td scope="row">{{ $i++ }}</td>
                    <td>{{ $dt->nama_barang }}</td>
                    <td>{{ $dt->nama_jenisbarang }}</td>
                    <td>{{ $dt->jumlah_masuk ? $dt->jumlah_masuk : "0"}} /Pcs</td>
                    <td>{{ $dt->stokterkini }} /Pcs</td>
                </tr>


            @endforeach
            <tr>
                <td colspan="3">
                    Total Barang 
                </td>
                <td></td>
                <td></td>
            </tr>

        </tbody>
    </table>

</body>

</html>
