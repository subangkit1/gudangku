@extends('admin.appadmin')
@section('konten')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="{{route('laporanmasukpost')}}" name="myForm">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="card-title">
                            <h5>Laporan Barang Masuk</h5>
                        </div>

                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <input type="radio" name="radio" id="semuadata" value="semuadata"> Semua Data
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="radio" name="radio" id="semuatanggal" value="tanggal"> Tanggal
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <div class="input-group input-group-sm mb-3">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Tanggal Awal</span>
                                        <input type="text" class="form-control" aria-label="Sizing example input"
                                            id="datepicker_awal" aria-describedby="inputGroup-sizing-sm"
                                            name="tanggal_awal">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <div class="input-group input-group-sm mb-3">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Tanggal Akhir</span>
                                        <input type="text" class="form-control" aria-label="Sizing example input"
                                            id="datepicker_akhir" aria-describedby="inputGroup-sizing-sm"
                                            name="tanggal_akhir">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="radio" name="radio" id="semuapegawai" value="pegawai"> Pegawai
                            </div>
                            <div class="col-md-4">
                                <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                                    name="petugas" id="penyedia">
                                    <option value="0" selected>Pilih</option>
                                    @foreach ($petugas as $pt)
                                        <option value="{{ $pt->id }}">{{ $pt->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">

                            </div>
                        </div>

                        <div class="row" style="margin-top:50px">
                            <div class="col-md-6">

                            </div>

                            <div class="col-md-6">
                                <button type="submit" class="btn btn-sm btn-primary" style="width:300px;float:right"> Cetak
                                </button>

                                <button type="submit" class="btn btn-sm btn-warning" style="width:300px;float:right">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

    {{-- tabel --}}
    {{-- end table --}}
@endsection
@section('js')
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet"
        media="screen">
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    {{-- date --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="text/javascript">
        $(document).ready(function() {
            // select2
            $('#jenisbarang, #penyedia').select2({
                //update here
                tags: true,
                placeholder: " -Pilih- ",
                allowClear: true,
            });
            // alert("test");
            $('#datepicker_awal').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yyyy-mm-dd',
            });

            $('#datepicker_akhir').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
            });
            $('#myTable').dataTable();

            //proses disabled form
            document.getElementById('datepicker_akhir').disabled = true;
            document.getElementById('datepicker_awal').disabled = true;
            $('select').prop('disabled', true);


            // kondisi radio 
            document.addEventListener('input',(e)=>{
                console.log(e.target.value);
                if(e.target.getAttribute('name') == 'radio' ){
                   //tanggal
                   if(e.target.value == 'tanggal'){
                        enableTanggal();
                   }else{
                       disableTanggal();
                   }
                   //pegawai
                   if(e.target.value == 'pegawai'){
                        enablePegawai();
                   }else{
                        disablePegawai();
                   }

                   


                }
            })

            
        })
        // table = new DataTable('#myTable');
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>

    <script>
        function generatelaporan() {
            alert("test");
        }
        function enableTanggal(){
            document.getElementById('datepicker_akhir').disabled = false;
            document.getElementById('datepicker_awal').disabled = false;
        }
        function disableTanggal(){
            document.getElementById('datepicker_akhir').disabled = true;
            document.getElementById('datepicker_awal').disabled = true;
        }
        function enablePegawai(){
            $('select').prop('disabled', false);
        }
        function disablePegawai(){
            $('select').prop('disabled', true);
        }

    </script>
@endsection
