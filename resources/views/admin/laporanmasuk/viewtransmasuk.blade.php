@extends('admin.appadmin')
@section('konten')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h6>TRANSAKSI MASUK</h6>
            </div>
        </div>
    </div>

</div>
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            
            <table class="table caption-top" id="myTable">
               <caption>Tabel Transaksi</caption>
               <thead>
                  <tr>
                     <th scope="col">NO</th>
                     <th scope="col">NAMA DIVISI</th>
                     <th scope="col">STATUS</th>
                     <th scope="col"></th>
                  </tr>
               </thead>
               <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                  
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
// alert("test");
$('#myTable').dataTable();
})
// table = new DataTable('#myTable');
   @if(session()->has('success'))
   toastr.success('{{ session('success') }}', 'BERHASIL!');
   @elseif(session()->has('error'))
     toastr.error('{{ session('error') }}', 'GAGAL!');
   @endif
</script>
@endsection