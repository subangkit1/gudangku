@extends('admin.appadmin')
@section('konten')
<div class="row">

<div class="row">
    <div class="col-xl-12 col-sm-12 col-12 d-flex">
      <div class="card bg-comman w-100">
         <div class="card-body">
             
            {{-- table user biasa --}}
            <div class="row">
               <div class="col-xl-12 col-sm-12 col-12 d-flex">
                  <div class="card bg-comman w-100">
                     <div class="card-body">
                        <div class="table-responsive">
                        <table class="table" id="validasi-arga">
                           <thead>
                         <tr>
                           <th scope="col">No</th>
                           <th scope="col">Nomer order</th>
                           <th scope="col">Pegawai</th>
                           <th scope="col">Nomer surat</th>
                           <th scope="col">Barang</th>
                           <th scope="col">Tanggal</th>
                           <th> Validasi</th>
                           <th></th>
                         </tr>
                       </thead>
                       <?php $i=1 ?>
                       <tbody>
                        @foreach ( $datas as $data )
                         <tr>
                           <th scope="row">{{ $i++ }}</th>
                           <td><span class="badge bg-dark">ORDER-{{ $data->id_pesanan ? $data->id_pesanan : '-' }}</span></td>
                           <td>{{ $data->pegawai->name }}</td>
                           <td>DL-{{ $data->keterangan }}</td>
                           <td>
                              @foreach ($data->bondetail  as $item)
                                 {{ $item->tobarang->nama_barang }} <span class="badge rounded-pill bg-secondary">{{ $item->jumlah }}/pcs</span> <br>
                              @endforeach
                           </td>


                           <td>{{\Carbon\Carbon::parse($data->tanggal_bon)->isoFormat('D MMMM Y')  }}</td>
                           <td>
                              @if ($data->status >= 2)
                              <span class="badge rounded-pill bg-success">Rikyan</span>
                              <span class="badge rounded-pill bg-info">Arga</span>
                              @elseif($data->status >= 1)
                              <span class="badge rounded-pill bg-success">Rikyan</span>
                              @else
                              <span class="badge rounded-pill bg-secondary">Belum approvement</span>
                              @endif
                           </td>
                           <td>

                              @if ($data->status >= 2)
                              <button class="btn btn-sm btn-primary" disabled onclick="getinfo({{ $data->id_pesanan }})"><i class="fa fa-info-circle"></i> Approve</button>
                              @elseif($data->status >= 1)
                              <button class="btn btn-sm btn-outline-primary" onclick="getinfo({{ $data->id_pesanan }})"><i class="fa fa-info-circle"></i> Approve</button>
                              @else
                              {{-- <span class="badge rounded-pill bg-secondary">-</span> --}}
                              @endif                        
                              </td>
                         </tr>     
                        @endforeach
                       </tbody>
                     
                       </table>
                       </div>

                     </div>
                  </div>
               </div>
               

            </div>

            {{-- detail dari bon --}}
            <div class="row">

               
            </div>
            
            
            {{-- end table user biasa --}}
           
            
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')

{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" media="screen"> --}}
{{-- <link href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css" rel="stylesheet" media="screen"> --}}

<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

{{-- <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script> --}}

<script type="text/javascript">
   $(document).ready(function(){
   $('#validasi-arga').dataTable();
   })
   // table = new DataTable('#myTable');
      @if(session()->has('success'))
      toastr.success('{{ session('success') }}', 'BERHASIL!');
      @elseif(session()->has('error'))
        toastr.error('{{ session('error') }}', 'GAGAL!');
      @endif
</script>


<script>
   function getinfo(id)
   {
      let token = $("meta[name='csrf-token']").attr("content");
      let id_header = id;
        $.ajax({
            url :`/admin-validasi/${id}`,
            type:"PUT",
            cache:false,
            data:{
                "id":id,
                "_token":token
            },
            success:function(data){
                console.log(data)
                window.location.replace("http://localhost:8000/admin-validasi");
            }
        });
   }

</script>

@endsection