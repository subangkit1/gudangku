@extends('admin.appadmin')
@section('konten')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('role.admin_user') }}" class="btn btn-sm btn-primary" style="margin-bottom: 10px"> Lihat
                        User</a>
                    <div class="table-responsive">
                        <table class="table table-stripped " id="myTable">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    {{-- <th scope="col">Nama</th> --}}
                                    <th scope="col">Email / Username</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">App</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($datarole as $data)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        {{-- <td>{{ $data->ambil_user->name }}</td> --}}
                                        <td>{{ $data->ambil_user->email }}</td>
                                        <td>
                                            @if ($data->role_id === 1)
                                                Admin
                                            @else
                                                User
                                            @endif
                                        </td>
                                        <td>{{ $data->model_type }}</td>
                                        <td>
                                            @if ($data->model_id === 3)
                                                {{-- status bu rikyan hard code --}}
                                                <button type="button" disabled class="btn btn-sm btn-primary"> Ubah
                                                </button>
                                            @else
                                                <button type="button" class="btn btn-sm btn-primary"> Ubah Role</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- end table --}}

    {{-- div untuk modal --}}
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
@endsection
@section('js')
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet"
        media="screen">
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    {{-- date --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="text/javascript">
        $(document).ready(function() {
            // select2
            $('#jenisbarang, #penyedia').select2({
                //update here
                tags: true,
                placeholder: " -Pilih- ",
                allowClear: true,
            });
            // alert("test");
            $('#datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
            });
            $('#myTable').dataTable();
        })
        // table = new DataTable('#myTable');
        @if (session()->has('success'))
            toastr.success('{{ session('success') }}', 'BERHASIL!');
        @elseif (session()->has('error'))
            toastr.error('{{ session('error') }}', 'GAGAL!');
        @endif
    </script>
@endsection
