@extends('layouts.app')

@section('content')
    <h1>Tambah Kategori Alat Baru</h1>

    <form action="{{ route('kategorialats.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="kodekategori">Kode Kategori</label>
            <input type="text" name="kodekategori" class="form-control">
        </div>
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" class="form-control">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
