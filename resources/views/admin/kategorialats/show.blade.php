@extends('layouts.app')

@section('content')
    <h1>Detail Kategori Alat</h1>

    <p><strong>Kode Kategori:</strong> {{ $kategorialat->kodekategori }}</p>
    <p><strong>Nama:</strong> {{ $kategorialat->nama }}</p>
    <p><strong>Status:</strong> {{ $kategorialat->status }}</p>

    <a href="{{ route('kategorialats.edit', $kategorialat->id) }}" class="btn btn-primary">Edit</a>

    <form action="{{ route('kategorialats.destroy', $kategorialat->id) }}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus kategori alat ini?')">Hapus</button>
    </form>
@endsection
