@extends('layouts.app')

@section('content')
    <h1>Daftar Kategori Alat</h1>

    <a href="{{ route('kategorialats.create') }}" class="btn btn-primary mb-2">Tambah Kategori Alat</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Kode Kategori</th>
                <th>Nama</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($kategorialats as $kategorialat)
                <tr>
                    <td>{{ $kategorialat->id }}</td>
                    <td>{{ $kategorialat->kodekategori }}</td>
                    <td>{{ $kategorialat->nama }}</td>
                    <td>{{ $kategorialat->status }}</td>
                    <td>
                        <a href="{{ route('kategorialats.show', $kategorialat->id) }}" class="btn btn-info btn-sm">Lihat</a>
                        <a href="{{ route('kategorialats.edit', $kategorialat->id) }}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('kategorialats.destroy', $kategorialat->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus kategori alat ini?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
