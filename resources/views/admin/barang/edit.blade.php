@extends('admin.appadmin')
@section('konten')
<div class="row">
   <div class="col-md-6">
      <div class="card" >
         <div class="card-body">
            <h5 class="card-title">Form Barang Edit</h5>
            <form action="{{ route('barang.update',$barang->id) }}" method="POST">
               {{ csrf_field() }}
               @method('PUT')
               {{-- <input class="form-control form-control-sm mb-2" type="text" name="barang_id" value="{{ $barang->id }}" aria-label=".form-control-sm example"> --}}
               <input class="form-control form-control-sm mb-2" type="text" name="nama_barang" value="{{ $barang->nama_barang }}" aria-label=".form-control-sm example">
               <select id="" name="jenis_barang" class="form-control mb-2">
                  @foreach ($datajenis as $dtjenis)
                     <option value="{{ $dtjenis->id }}" {{ $dtjenis->id == $barang->jenis_barang_id ? 'selected' : '' }}>{{ $dtjenis->nama_jenisbarang }}</option>
                  @endforeach
                  
               </select>
               <input class="form-control form-control-sm mb-2" type="text" name="jumlah" value="{{ $barang->jumlah }}" aria-label=".form-control-sm example">
               <input class="form-control form-control-sm mb-2" type="text" name="harga" value="{{ $barang->harga }}" aria-label=".form-control-sm example">
               <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
            </form>
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@endsection