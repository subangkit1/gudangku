@extends('admin.appadmin')
@section('konten')
<div class="row">
   <div class="col-md-6">
      <div class="card">
         <div class="card-body">
            <h6>FORM BARANG MASUK </h6>
         </div>
      </div>
   </div>

   <div class="col-md-6">
      <div class="card" style="background-color:#">
         <div class="card-body">
            <a href="{{ route('transaksimasuk.view') }}" target="_blank" rel="noopener noreferrer" class="btn btn-md btn-info" style="color:aliceblue"><i class="fa fa-eye"></i>Transaksi</a>
         </div>
      </div>
   </div>
</div>
{{-- header pertama  --}}
<form action="{{ route('transaksimasuk.store') }}" method="POST">
   {{ csrf_field() }}
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               {{-- isi card --}}
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group input-group-sm">
                        <label>Nomer:</label>
                        <input type="text" value="1" class="form-control form-control form-control-sm" readonly name="nomer">
                     </div>
                     <div class="form-group">
                        {{-- select 2 --}}
                        <label>Diterima dari: <span id="tambahpenyedia" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#centermodal"> <i class="fa fa-plus"></i></span></label>
                        <select class="form-control form-control" name="penyedia" id="penyedia">
                           <option value="0"> -- Pilih -- </option>
                           @foreach ($penyedia as $orang)
                           <option value="{{ $orang->id }}">{{ $orang->nama_pemasok }}</option>
                           @endforeach
                        </select>
                        {{-- end select --}}
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Tanggal :</label>
                        <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="tanggal" id="datepicker" autocomplete="off" required>
                     </div>
                     <div class="form-group">
                        <label>Nomer PO:</label>
                        <input type="text" class="form-control form-control form-control-sm" name="nomerpo" required>
                     </div>
                  </div>
               </div>
               {{-- end isi card --}}
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               {{-- perhitungannya disini form dinamis  --}}
               <div class="table-resposive">
                  <table class="table table-striped mb-0">
                     <thead>
                        <tr>
                           <th>NAMA BARANG</th>
                           </th>
                           <th>JUMLAH</th>
                           <th>SATUAN</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody id="insertdisini">
                        <tr >
                           <td>
                              <select class="form-control form-control-sm js-example-basic-single" name="barang_id[]" style="width:100%">
                                 @foreach ($barang as $itembarang)
                                 <option value="{{ $itembarang->id }}">{{ $itembarang->nama_barang }}</option>
                                 @endforeach
                              </select>
                           </td>
                           <td><input type="number" name="jumlah[]" min="1" class="form-control form-control-sm"></td>
                           <td>PCS</td>
                           <td><button class="btn btn-sm btn-primary" id="tambah"><i class="fa fa-plus"></i></button></td>
                        </tr>
                        {{-- <span id="insertdisini">
                        </span> --}}
                     </tbody>
                  </table>
                  <input type="submit" value="SIMPAN" class="btn btn-primary btn-sm mt-2" style="margin-right:5px"><input type="submit" value="BERSIHKAN" class="btn btn-danger btn-sm mt-2" >
</form>
</div>
{{-- inputan dinammis  --}}
</div>
</div>
</div>
</div>
{{-- modal dialog --}}
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body">
            {{-- isi  --}}
            <div class="modal fade" id="centermodal" tabindex="-1" aria-hidden="true" style="display: none;">
               <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title" id="myCenterModalLabel">
                           <h5 class="card-title">Form Penyedia</h5>
                        </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                     </div>
                     <div class="modal-body">
                        <form action="{{ route('pemasok.store') }}" method="POST">
                           {{ csrf_field() }}
                           <input class="form-control form-control-sm mb-2" type="text" name="namapemasok" placeholder="Nama Pemasok" aria-label=".form-control-sm example">
                           <input class="form-control form-control-sm mb-2" type="text" name="telp" placeholder="Phone (contoh : 08213999112X)" aria-label=".form-control-sm example">
                           <input class="form-control form-control-sm mb-2" type="text" name="alamat" placeholder="Alamat Lengkap" aria-label=".form-control-sm example">
                           <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            {{-- end isi --}}
         </div>
      </div>
   </div>
</div>
{{-- end modal --}}
{{-- end header pertama --}}
{{-- header kedua --}}
{{-- end header kedua --}}
{{-- tabel terakhir --}}
{{-- end tabel terakhir  --}}
@endsection
@section('js')
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script type="text/javascript">
   // $.fn.datepicker.defaults.format = "mm/dd/yyyy";
   function selectRefresh() {
     $('.js-example-basic-single').select2({
       //-^^^^^^^^--- update here
       tags: true,
       placeholder: "Select an Option",
       allowClear: true,
     });
   
   }
      $(document).ready(function(){
   
      $('#penyedia').select2({
       //update here
       tags: true,
       placeholder: "Select an Option",
       allowClear: true,
     });
         $('#datepicker').datepicker();
      
      var cTime = moment(new Date()).format("YYYY-MM-DD");
      console.log("sudah install moment js "+cTime);
       $('.js-example-basic-single').select2();
       // tambah row inputan 
       $('#tambah').click(function(event){
         event.preventDefault();
           var html = '';
           var kotak = $("#kotak");
           html = `<tr id="rowku">
                               <td>
                                   <select class="form-control form-control-sm js-example-basic-single" name="barang_id[]" style="width:100%">
                                       @foreach ($barang as $itembarang)
                                       <option value="{{ $itembarang->id }}">{{ $itembarang->nama_barang }}</option>     
                                       @endforeach
                                       </select>
                               </td>
                               <td><input type="number" min="1" name="jumlah[]" class="form-control form-control-sm" required></td>
                               <td>PCS</td>
                               <td><button class="btn btn-sm btn-danger" id="hapus"><i class="fa fa-trash"></i></button></td>
                           </tr>`;
           $('#insertdisini').append(html);
           selectRefresh();
   
   
           
       });
       //end tambah inputan 
   
       // remove row 
      $('#insertdisini').on('click','#hapus',function(e){
       e.preventDefault();
        $(this).parent().parent().remove();
       
      })
   
      
      $('#myTable').dataTable();
      })
      // table = new DataTable('#myTable');
         @if(session()->has('success'))
         toastr.success('{{ session('success') }}', 'BERHASIL!');
         @elseif(session()->has('error'))
           toastr.error('{{ session('error') }}', 'GAGAL!');
         @endif
</script>
@endsection