<!-- resources/views/provinsis/edit.blade.php -->

@extends('admin.appadmin')

@section('konten')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    

    <h5>Edit Provinsi</h5>

    <form action="{{ route('provinsis.update', $provinsi->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama_provinsi">Nama Provinsi</label>
            <input type="text" name="nama_provinsi" class="form-control" value="{{ $provinsi->nama_provinsi }}">
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control" value="{{ $provinsi->status }}">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
