<!-- resources/views/provinsis/show.blade.php -->

@extends('admin.appadmin')

@section('konten')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
    <h1>Detail Provinsi</h1>

    <p><strong>Nama Provinsi:</strong> {{ $provinsi->nama_provinsi }}</p>
    <p><strong>Status:</strong> {{ $provinsi->status }}</p>

    <a href="{{ route('provinsis.edit', $provinsi->id) }}" class="btn btn-primary">Edit</a>

    <form action="{{ route('provinsis.destroy', $provinsi->id) }}" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus provinsi ini?')">Hapus</button>
    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
