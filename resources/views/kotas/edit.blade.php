@extends('admin.appadmin')

@section('konten')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h6>Edit Kota</h6>
                    
                        <form action="{{ route('kotas.update', $kota->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="prov_id">Provinsi</label>
                                <select name="prov_id" class="form-control">
                                    @foreach($provinsis as $provinsi)
                                        <option value="{{ $provinsi->id }}" @if($provinsi->id == $kota->prov_id) selected @endif>{{ $provinsi->nama_provinsi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="nama_kota">Nama Kota</label>
                                <input type="text" name="nama_kota" class="form-control" value="{{ $kota->nama_kota }}">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="text" name="status" class="form-control" value="{{ $kota->status }}">
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                </div>
            </div>
            
        </div>
    </div>

@endsection
