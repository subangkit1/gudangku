@extends('admin.appadmin')

@section('konten')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                                
                <h5>Detail Kota</h5>
                <p><strong>Provinsi:</strong> {{ @$kota->provinsi->nama_provinsi }}</p>
                <p><strong>Nama Kota:</strong> {{ $kota->nama_kota }}</p>
                <p><strong>Status:</strong> {{ $kota->status }}</p>

                <a href="{{ route('kotas.edit', $kota->id) }}" class="btn btn-primary">Edit</a>

                <form action="{{ route('kotas.destroy', $kota->id) }}" method="POST" style="display: inline;">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus kota ini?')">Hapus</button>
                </form>

                </div>
            </div>
        </div>
    </div>
@endsection
