@extends('admin.appadmin')

@section('konten')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h6>Tambah Kota Baru</h6>

                    <form action="{{ route('kotas.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="prov_id">Provinsi</label>
                            <select name="prov_id" class="form-control">
                                @foreach($provinsis as $provinsi)
                                    <option value="{{ $provinsi->id }}">{{ $provinsi->nama_provinsi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama_kota">Nama Kota</label>
                            <input type="text" name="nama_kota" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <input type="text" name="status" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>

                </div>
            </div>
            
        </div>
    </div>
@endsection
