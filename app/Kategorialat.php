<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategorialat extends Model
{
    protected $table = 'tb_kategorialat';
    protected $primaryKey = 'id';
    protected $fillable = ['id','kodekategori','nama','status'];//isi tabel
    public $timestamps = false;

}
