<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelalat extends Model
{
    protected $table = 'tb_alat';
    protected $primaryKey = 'id';
    protected $fillable = ['id','nama_alat','jam_alat','satuanoutpud_id','kategorialat_id','status'];//isi tabel
    public $timestamps = false;
   
}
