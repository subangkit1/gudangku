<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModelBarang as barang;
use App\Models\User;
use App\Models\rumahsakit\ModelPermohonankalibrasi as permohonan;
use App\Models\Modeltransaksiprioritas as prioritas;
use App\Models\Modelbonbarang as bon;
use App\Models\Modelpegawaikal as peg;
use App\Models\rumahsakit\Modelpermohonandetail as permohonandetail;
use Auth;
use DB;
use App\Models\ModelKeahlianPegawai as kp;
use App\Models\Modelpermohonanpegawai as ppegawai;
//

class Usercontroller extends Controller
{
    public function index(){
        // Mengambil semua data barang dan mengelompokkannya berdasarkan 'nama_barang' dan 'jumlah'
        $data = barang::all()->pluck('nama_barang','jumlah');
        // Mengambil nilai (nama barang) dari data yang dihasilkan
        $nama = $data->values();
        // Mengambil kunci (jumlah) dari data yang dihasilkan
        $jumlah = $data->keys();
       
        // Menghitung jumlah total bon
        $bon = bon::count();
        // Menghitung jumlah bon yang telah disetujui (status = 1)
        $bonacc = bon::where('status','1')->count();
        // Menghitung jumlah bon yang tidak disetujui (status = 0)
        $bonnoacc = bon::where('status','0')->count();
        // Memeriksa apakah pengguna yang login memiliki role pelayanan teknis (id = 3)
        if(Auth()->user()->id == 3){ 
            // Jika ya, kembalikan view 'user.index' dengan data yang relevan
            return view('user.index',compact('bonacc','bonnoacc','bon','nama','jumlah'));

        }else{
            // Jika bukan, ambil id pengguna yang login
            $user_id = auth::user()->id;
            // Ambil data bon berdasarkan 'pegawai_id' dan urutkan berdasarkan 'id_pesanan' secara descending
            $bon = bon::where('pegawai_id',$user_id)->orderBy('id_pesanan','DESC')->get();
            // Kembalikan view 'user.index' dengan data yang relevan
            return view('user.index',compact('nama','jumlah','bon'));
        }
        
    }

    // Fungsi ini digunakan untuk menampilkan data pegawai dan permohonan kalibrasi untuk rumah sakit
    public function userKalibrasiRS(){

        // dd("test");
        // Mengambil data pegawai diurutkan berdasarkan nama secara ascending
        $pegawai = peg::orderBy('nama','ASC')->get(); 
        // Mengambil data permohonan kalibrasi diurutkan berdasarkan id secara descending
        $permohonan = permohonan::orderBy('id','DESC')->get(); 
        // Mengembalikan view beserta data pegawai dan permohonan
        return view('user.rumahsakit.index', compact('permohonan','pegawai')); 
    }

    public function userkalibrasistore(Request $request){
    //    dd($request->all());

        $pegawaiIds = $request->input('pegawai_id'); // Mendapatkan ID pegawai dari inputan
        $startDate = $request->input('start_date'); // Mendapatkan tanggal mulai dari inputan
        $endDate = $request->input('end_date'); // Mendapatkan tanggal akhir dari inputan
        $permohonanId = (int) $request->input('permohonan_id'); // Mendapatkan ID permohonan dari inputan

        // dd($startDate);

        // to string
        // $startDate = $startDate[0];


        $up_statuspermohonan = permohonan::find($permohonanId);
        $up_statuspermohonan->status = '1';
        $up_statuspermohonan->save();


        $dataToSave = []; // Inisialisasi array untuk menyimpan data
        foreach ($pegawaiIds as $pegawaiId) { // Melakukan iterasi untuk setiap ID pegawai
            $period = new \DatePeriod( // Membuat periode tanggal dari tanggal mulai hingga tanggal akhir
                new \DateTime($startDate), // Tanggal mulai
                new \DateInterval('P1D'), // Interval setiap hari
                (new \DateTime($endDate))->modify('+1 day') // Tanggal akhir ditambah 1 hari untuk inklusif
            );

            foreach ($period as $date) { // Melakukan iterasi untuk setiap tanggal dalam periode
                $dataToSave[] = [ // Menambahkan data ke array
                    'pegawai_id' => $pegawaiId, // ID pegawai
                    'tanggal' => $date->format("Y-m-d"), // Tanggal dalam format Y-m-d
                    'permohonan_id' => $permohonanId, // ID permohonan
                    'status' => 0 // Status awal diatur ke 0
                    
                ];
            }
        }
        $statusnya = DB::table('permohonan_pegawai')->insert($dataToSave);
        dd($statusnya);




       /*
       $pegawaiIds = $request->input('pegawai_id'); // Mendapatkan ID pegawai dari inputan
        $startDate = $request->input('start_date'); // Mendapatkan tanggal mulai dari inputan
        $endDate = $request->input('end_date'); // Mendapatkan tanggal akhir dari inputan
        $permohonanId = $request->input('permohonan_id'); // Mendapatkan ID permohonan dari inputan

        // update permohonan id mejadi status 1;
        $up_statuspermohonan = permohonan::find($permohonanId);
        $up_statuspermohonan->status = '1';
        $up_statuspermohonan->save();

        $dataToSave = []; // Inisialisasi array untuk menyimpan data
        foreach ($pegawaiIds as $pegawaiId) { // Melakukan iterasi untuk setiap ID pegawai
            $period = new \DatePeriod( // Membuat periode tanggal dari tanggal mulai hingga tanggal akhir
                new \DateTime($startDate), // Tanggal mulai
                new \DateInterval('P1D'), // Interval setiap hari
                (new \DateTime($endDate))->modify('+1 day') // Tanggal akhir ditambah 1 hari untuk inklusif
            );

            foreach ($period as $date) { // Melakukan iterasi untuk setiap tanggal dalam periode
                $dataToSave[] = [ // Menambahkan data ke array
                    'pegawai_id' => $pegawaiId, // ID pegawai
                    'tanggal' => $date->format("Y-m-d"), // Tanggal dalam format Y-m-d
                    'permohonan_id' => $permohonanId, // ID permohonan
                    'status' => 0 // Status awal diatur ke 0
                    
                ];
            }
        }
        $statusnya = DB::table('permohonan_pegawai')->insert($dataToSave);
       
       */
    }

    // cek permohonan pegawai berdasarkan keahlian
    public function cekpermohonandl($id){
        // dd($id);
        $permohonan = permohonan::find($id);
        $permohonan_detail = permohonandetail::where('permohonan_id',$permohonan->id)->pluck('alat_id');
        // dd($permohonan_detail);
        $permohonan_idku = $permohonan->id;

        $ids = [];
        $ids = $permohonan_detail;
        // dd($ids);

        $data = kp::whereIn('alat_id', $ids)->get();
        $map = $data->map(function ($item,$value)use($id){
            $item->namaPegawai = peg::where('id',$item->petugas_id)->get();
            $item->permohanan_id = $id;
            return $item;
        });
        // end gpt

        // return $map;
        return response()->json($map);
    }

    // Fungsi ini digunakan untuk mengambil data pegawai dan mengembalikannya dalam format JSON
    public function pegawaiData(){
        // Mengambil data pegawai dengan memilih kolom id, nama, dan divisi_id
        $pegawai = peg::select('id', 'nama as name','user_id', 'divisi_id as position')->get(); 
        // Mengembalikan data pegawai dalam format JSON
        return response()->json(['data' => $pegawai]); 


    }

    public function savePegawai(Request $request){ //simpan pengajuan pegawai
        $pegawaiIds = $request->input('pegawai_id'); // Mendapatkan ID pegawai dari inputan
        $startDate = $request->input('start_date'); // Mendapatkan tanggal mulai dari inputan
        $endDate = $request->input('end_date'); // Mendapatkan tanggal akhir dari inputan
        $permohonanId = $request->input('permohonan_id'); // Mendapatkan ID permohonan dari inputan

        // update permohonan id mejadi status 1;
        $up_statuspermohonan = permohonan::find($permohonanId);
        $up_statuspermohonan->status = '1';
        $up_statuspermohonan->save();

        $dataToSave = []; // Inisialisasi array untuk menyimpan data
        foreach ($pegawaiIds as $pegawaiId) { // Melakukan iterasi untuk setiap ID pegawai
            $period = new \DatePeriod( // Membuat periode tanggal dari tanggal mulai hingga tanggal akhir
                new \DateTime($startDate), // Tanggal mulai
                new \DateInterval('P1D'), // Interval setiap hari
                (new \DateTime($endDate))->modify('+1 day') // Tanggal akhir ditambah 1 hari untuk inklusif
            );

            foreach ($period as $date) { // Melakukan iterasi untuk setiap tanggal dalam periode
                $dataToSave[] = [ // Menambahkan data ke array
                    'pegawai_id' => $pegawaiId, // ID pegawai
                    'tanggal' => $date->format("Y-m-d"), // Tanggal dalam format Y-m-d
                    'permohonan_id' => $permohonanId, // ID permohonan
                    'status' => 0 // Status awal diatur ke 0
                    
                ];
            }
        }
        $statusnya = DB::table('permohonan_pegawai')->insert($dataToSave);
        if ($statusnya) {
            return response()->json(['success' => true, 'message' => 'Data pegawai berhasil disimpan.'], 200);
        } else {
            return response()->json(['success' => false, 'message' => 'Terjadi kesalahan saat menyimpan data pegawai.'], 500);
        }
    }

    /*
    
    
    */


    public function stokbaranguser(){
        $databrang = barang::all();
        // dd($databarang);
    }

    public function getpermohonanpegawai(){
        $permohonan = permohonan::where('status','1')->orderBy('id','DESC')->get();
        // dd($permohonan);
        return view('user.riwayatpermohonanrs.index',compact('permohonan'));
    }

}
