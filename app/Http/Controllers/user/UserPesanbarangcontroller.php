<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModelBarang as barang;
use App\Models\Modelbonbarang as bon;
use App\Models\Modelbondetail as bondetail;
use App\Models\ModelSatuanjenis as jenis;
use Auth;

class UserPesanbarangcontroller extends Controller
{
    public function index(){
        $count = bon::count();
        // dd($count);
        $barang = barang::orderBy('nama_barang','ASC')->get();
        return view('user.bonbarang.index',compact('barang','count'));
    }

    public function store(Request $request,barang $barang){
        // dd($request->all());
        /* 
        status = 0 barang belum di commit, 1 barang di approve, 2 barang di update stoknya 
        (dengan keterangan apakah stok sesuai dengan yang diminta )
        Stok barang kurang dari 10 tdaik bisa dipesan 
        */
        $pegawai_id = Auth::user()->id;

        $jumlahArray = count($request->barang_id);
        //pengecekkan stok ? apakah stok lebih besar dari stok gudang 
        for($i=0;$i<$jumlahArray;$i++){
            $barang = barang::where('id',$request->barang_id[$i])->first();
            $stok = $barang->jumlah;
            if($request->jumlah[$i] >= $stok){ //inputan lebih besar
                dd("data stok lebih kecil ");
            }
        }
        //insert tabel header
        $headerPesan =  bon::create([
            'pegawai_id' => $pegawai_id,
            'tanggal_bon' => $request->tanggal_pesanan,
            'status' => '0', //status belum di approve sama bu rikyan 
            'keterangan' => $request->keterangan_pesanan
        ]);
        // dd($headerPesan->id_pesanan);
        //insert detail
        for($a=0;$a<count($request->barang_id);$a++){
            $detailPesan = bondetail::create([
                        'bon_barang_id' => $headerPesan->id_pesanan,
                        'barang_id' => $request->barang_id[$a],
                        'jumlah' => $request->jumlah[$a],
                        'status' => 0 //status barang belum tercommit
                    ]);
        }
        return redirect()->route('user.pesan')->with(['success' => 'Data Berhasil di Pesan!']);
     
    }

    public function lihatorder(){
        $data = bon::orderBy('id_pesanan','DESC')->get();
        return view('user.bonbarang.lihatbarang',compact('data'));
    }
    public function lihatdetailorder(Request $request){
        $barang = barang::all();
        $data = bondetail::where('bon_barang_id',$request->id)->get();
        $map = $data->map(function ($row,$key) use ($barang){
            $rowtobarang = $row->tobarang->jenisbarang[0]->nama_jenisbarang;
            $row->namabarang = barang::where('id',$row->barang_id)->first();
            return $row;
        });
        return response()->json($map);
    }

    public function validasi($id){
        //cek user jika user loggin sebagai role bu rikyan 
        //update tabel status = 1
        $bondetail = bondetail::where('bon_barang_id',$id)->get();

        for($i=0;$i<count($bondetail);$i++){
                $datadetail = barang::where('id',$bondetail[$i]->barang_id)->first(); //ambil data barang berdasarkan barang id detail bon
                $datadetail->jumlah = $datadetail->jumlah - $bondetail[$i]->jumlah; //kurangi stok pada masterbarang dengan detail nya
                $statusstok = $datadetail->save(); 
        }
        //update status bon baranhg
        $updatebon = bon::find($id);
        $updatebon->status = '1';
        $status = $updatebon->save();

        //status tabel detail belum di update
        if($status){
            return response()->json([
                'status_code' => 200,
                'message' => 'sukses'
            ]);
        }
    }

    public function statusbon($id){
        $data = bon::where('id_pesanan',$id)->get();
        return response()->json($data);
    }

    


}
