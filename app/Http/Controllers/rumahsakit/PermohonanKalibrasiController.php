<?php

namespace App\Http\Controllers\rumahsakit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rumahsakit\ModelPermohonanKalibrasi as permohonan;
use App\Models\Rumahsakit\Modelpermohonandetail as permohonan_detail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Modelalat as alat;
use App\Models\Rumahsakit\Modeltransaksiprioritas as transaksi_prioritas;
// use App\Models\Modeltransaksiprioritas as transaksi_prioritas;
use App\Models\Modelpermohonanpegawai as ppegawai;
use App\Models\rumahsakit\Modelrumahsakit as rs;

use App\Models\Modelpegawaikal as peg;
use Carbon\Carbon;


class PermohonanKalibrasiController extends Controller
{

    
    public function index()
    {
        $alat = alat::all();
        $user = Auth::user();
        // dd($user->name);
        return view('rumahsakit.permohonan.index',compact('alat','user'));
    }

    // json datanya bro 
    public function getdata(){
        $data = alat::all();
        return response()->json($data);
    }
    // detailnyaa 
    public function getdatadetail(Request $request){
        $data = alat::find($request->id);
        return response()->json($data);
    }

    //getharga
    public function getharga($alat_id){
        if($alat_id == "0"){
            return response()->json(['harga' => 0]);
        }else{
            $alat = alat::findOrFail($alat_id);
            try {
                if($alat->harga == "0") {
                    // throw new \Exception("Harga alat tidak boleh 0");
                    return response()->json(['harga' => 0]);
                }
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 400);
            }
            return response()->json(['harga' => $alat->harga]);
        }
    }

    public function create()
    {
        //
    }

 
    public function store(Request $request)  //save permohonan
    {
       $rumahsakit = Auth::user();
       $nomorsurat_rs = $request['requestHeader']['nomorsurat'];
       $p1 = $request['requestHeader']['paragraph'];
       $tgl_insert = Carbon::now();
       $catatan = 'kosong';

       $headerpermohonan = permohonan::create([
        'nomorsurat_rs' => @$nomorsurat_rs,
        'rumahsakit_id' => @$rumahsakit->id,
        'p1' => @$p1,
        'tgl_insert' => @$tgl_insert
       ]);

    //    dd($headerpermohonan->id);

       
        if(count($request->alat) < 2){
            $pointKritis = 0;
            $pointNonKritis = 0;
            $pointJamAlat = 0;
            $totalJamAlat = 0;
            $totalkritis = 0;

            //permohonan detail 
            $save_detail = permohonan_detail::create([
                'permohonan_id' => $headerpermohonan->id,
                'alat_id' => $request->alat[0]['nama_alat'],
                'catatan' => 'kosong',
                'qty' =>  $request->alat[0]['qty'],
                'harga' => $request->alat[0]['harga']
            ]);

            //prioritas single array
            $alat = alat::find($request->alat[0]['nama_alat']);
            if($alat->satuanoutpud_id == 1){
                $pointKritis = $request->alat[0]['qty'] * 100;
            }else{
                $pointKritis = $request->alat[0]['qty'];
            }

            $pointJamAlat = $alat->jam_alat * $request->alat[0]['qty'];
            $totalJamAlat = $totalJamAlat + $pointJamAlat; //totalJamalat                
            $totalkritis = $totalkritis + $pointKritis; //totalKritis
            $dummyLong = 0; //dummy longtitude

            $saveTransaksiPrioriatas = transaksi_prioritas::create([
                'transaksi_id' => $headerpermohonan->id,
                'prioritas_satu' => $totalJamAlat,
                'prioritas_dua' => $totalkritis,
                'prioritas_tiga' => $dummyLong,
                'tgl_insert' => Carbon::now()
            ]);

            if($saveTransaksiPrioriatas){
                return response()->json($saveTransaksiPrioriatas);
            }

        }elseif(count($request->alat) > 1){

            try {
                $pointKritis = 0;
                $pointNonKritis = 0;
                $pointJamAlat = 0;
                $totalJamAlat = 0;
                $totalkritis = 0;
                foreach ($request->alat as $key => $value) {
                    
                    $alat = alat::find($value['nama_alat']);
                    
                    if($alat->satuanoutpud_id == 1){
                        $pointKritis = $value['qty'] * 100;
                    }else{
                        $pointKritis = $value['qty'];
                    }
    
                    $pointJamAlat = $alat->jam_alat * $value['qty'];
                    $totalJamAlat = $totalJamAlat + $pointJamAlat; //totalJamalat                
                    $totalkritis = $totalkritis + $pointKritis; //totalKritis
                    $dummyLong = 0; //dummy longtitude
    
                    $save_detail = permohonan_detail::create([
                        'permohonan_id' => $headerpermohonan->id,
                        'alat_id' => $value['nama_alat'],
                        'catatan' => 'kosong',
                        'qty' =>  $value['qty'],
                        'harga' => $value['harga']
                    ]);
                    
                }
                //tb_prioritas_transaksi
                $saveTransaksiPrioriatas = transaksi_prioritas::create([
                    'transaksi_id' => $headerpermohonan->id,
                    'prioritas_satu' => $totalJamAlat,
                    'prioritas_dua' => $totalkritis,
                    'prioritas_tiga' => $dummyLong,
                    'tgl_insert' => Carbon::now()
                ]);

                if($saveTransaksiPrioriatas){
                    return response()->json($saveTransaksiPrioriatas);
                }
                
            } catch (\Throwable $th) {
                //throw $th;
            }
        
         }

        }
    


    public function riwayat(){
        // dd("test");
        $id = auth()->user();
        // dd($id);
        $monitoringpermohonan;
        $totalalatterkalibrasi;


        $permohonankalibrasi = permohonan::orderBy('id','DESC')->get();
        return view('rumahsakit.riwayat.index',compact('permohonankalibrasi'));
    }

    public function riwayatpetugas(){ //cekjadwal
        $user = Auth::user();
        // dd($user);
        try {
            $query = DB::table('permohonan_pegawai')
                ->select('permohonan_id', DB::raw('GROUP_CONCAT(DISTINCT pegawai_id) as pegawai_ids'))
                ->whereExists(function ($query) use ($user) {
                    $query->select(DB::raw(1))
                        ->from('users')
                        ->whereRaw('users.id = permohonan_pegawai.pegawai_id')
                        ->where('users.id', @$user->id); // Filter berdasarkan ID pengguna yang terautentikasi
                })
                ->groupBy('permohonan_id')
                ->orderBy('permohonan_id','DESC')
                ->get();
                // if(empty($query)){
                //     dd("data kosong");  
                // }
                // dd($query);
        $mapquery = $query->map(function($i,$v){
            $i->permohonan = permohonan::where('id',@$i->permohonan_id)->first();
            $i->rs = rs::where('id',@$i->permohonan->rumahsakit_id)->first();
            $i->tanggalkalibrasi = ppegawai::where('permohonan_id',@$i->permohonan->id)->where('pegawai_id',$i->pegawai_ids)->get();
            return $i;
        });
        // dd($mapquery);
        } catch (\Throwable $th) {
            throw $th;
        }

        if(count($mapquery)>0){
            // dd($mapquery);
            return view('user.riwayatpermohonanpetugas.index',compact('mapquery'));
        }else{
            return view('user.riwayatpermohonanpetugas.index',compact('mapquery'));
        }

    }


    public function cetakST(Request $request){
        // dd($request->all());

        // nomor surat  tabel surat isinya permohonan_id,nomor_surat ( isi manual diisi bu rikyan )
        // $nomor;

        $permohonan = permohonan::find($request->input('permohonan_id'));
        $rumahsakit = rs::find($permohonan->rumahsakit_id);
        // dd($rumahsakit);
        $pegawai = ppegawai::select('pegawai_id')
                    ->distinct('pegawai_id') // Hanya mengambil pegawai_id yang unik
                    ->where('permohonan_id',$request->input('permohonan_id'))
                    // ->orderBy('nama', 'ASC') // Misalnya, jika Anda ingin mengurutkan hasil berdasarkan nama ASC
                    ->get();
        // dd($pegawai);

        $map = $pegawai->map(function($item,$value){
            $item->pegawai = peg::where('user_id',$item->pegawai_id)->first();
            // dd($item->pegawai_id);
            return $item;
        });
        // dd($map);
        // dd($map[0]->pegawai->nama);
        return view('rumahsakit.surattugas.index',compact('permohonan','rumahsakit','map'));

        // ambil data alat transaksi 
        // nama petugas
        // bu rikyan
        // tanggal surat tugas 

        
    }


}
