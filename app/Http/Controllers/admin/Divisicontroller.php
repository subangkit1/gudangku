<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modeldivisi as divisi;

class Divisicontroller extends Controller
{
    
    public function index() //menampilkan ke view
    {
        $data = divisi::all();
        return view('admin.divisi.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    
    public function store(Request $request) //save data input 
    {
        
        $this->validate($request,[
            'namadivisi' => 'required|min:3'
        ]);

        $data = divisi::create([
            'nama_divisi' => $request->namadivisi,
            'status' => '1',
        ]);
        if($data){
            return redirect()->route('divisi.index')->with(['success' => 'Data Berhasil Disimpan']);
        //  return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);   
        }else{
            return redirect()->route('divisi.index')->with(['error' => 'Data Gagal Disimpan']);
        }


        
    }

    
    public function show($id) //menampilkan ketika id dipanggil
    {
        
    }

    
    public function edit(divisi $divisi) //metode ini menggunakan Dependency Injection santrikoding
    {
        return view('admin.divisi.edit',compact('divisi'));
    }

    
    public function update(Request $request, divisi $divisi) //update data berdasarkan id
    {
        //validasi form 
        $this->validate($request,[
            'namadivisi' => 'required|min:3'
        ]);
        // update
        $update = $divisi->update([
            'nama_divisi' => $request->namadivisi
        ]); 
        if($update){
            return redirect()->route('divisi.index')->with(['success' => 'Data Berhasil Diubah!']);
        }


    }

    
    public function destroy(divisi $divisi) //menghapus data
    {
        $status = $divisi->delete();
        if($status){
             return redirect()->route('divisi.index')->with(['success' => 'Data Berhasil Dihapus!']);
        }
    }
}
