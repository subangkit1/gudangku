<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Carbon\Carbon;

class Laporankeluarcontroller extends Controller
{
    function index(){
        $petugas = DB::select('select user.id,user.name from users user join model_has_roles hr on user.id = hr.model_id order by hr.model_id DESC');
        return view('admin.laporankeluar.index',compact('petugas'));
    }

    function laporanpost(Request $request){
        if(!isset($request->radio)){
            dd("variabel belum diset");
        }else{
            if($request->radio == "semuadata"){
                // dd("semuadata");
                $data = DB::select("select bb.id_pesanan,usr.name,nb.nama_barang,bd.jumlah,bb.tanggal_bon,bb.status,
                CASE
                when bb.status = 1 then 'Rikyan'
                when bb.status = 2 then 'Arga'
                ELSE 'Sabar'
                END AS acc
                from tb_bon_barang bb 
                join tb_bon_detail bd on bb.id_pesanan = bd.bon_barang_id 
                join tb_nama_barang nb on bd.barang_id = nb.id
                join users usr on bb.pegawai_id = usr.id
                order by bb.status DESC
                ");
                $keluartotal = DB::select('select sum(jumlah) as total from tb_bon_detail');
                // dd($keluartotal[0]->total);
                // dd($keluartotal[0]->total);
                
                $datas = [
                    'data' => $data,
                    'keluartotal' => $keluartotal
                ];
                $pdf = PDF::loadView('admin.laporankeluar.viewlaporan',$datas);
                return $pdf->download('BarangKeluar.pdf');
                // return view('admin.laporankeluar.viewlaporan',compact('data','keluartotal'));
                

            }elseif($request->radio == "tanggal"){
                $data = $request->all();
                $tgl_awal = Carbon::parse($request->tanggal_awal)->format('Y-m-d');
                $tgl_akhir = Carbon::parse($request->tanggal_akhir)->format('Y-m-d');
                // dd($tgl_akhir);
                $data = DB::select("select bb.id_pesanan,usr.name,nb.nama_barang,bd.jumlah,bb.tanggal_bon,bb.status,
                CASE
                when bb.status = 1 then 'Rikyan'
                when bb.status = 2 then 'Arga'
                ELSE 'Sabar'
                END AS acc
                from tb_bon_barang bb 
                join tb_bon_detail bd on bb.id_pesanan = bd.bon_barang_id 
                join tb_nama_barang nb on bd.barang_id = nb.id
                join users usr on bb.pegawai_id = usr.id
                where bb.tanggal_bon BETWEEN '$tgl_awal' and '$tgl_akhir'
                order by bb.status DESC");
                $keluartotal = DB::select("select sum(bd.jumlah) as total from tb_bon_barang bb join tb_bon_detail bd on bb.id_pesanan = bon_barang_id 
                where bb.tanggal_bon BETWEEN '$tgl_awal' and '$tgl_akhir'");
                // dd($keluartotal[]);
                $datas = [
                    'data' => $data,
                    'keluartotal' => $keluartotal
                ];
                $pdf = PDF::loadView('admin.laporankeluar.viewlaporan',$datas);
                return $pdf->download('BarangKeluar.pdf');
            }else{
                // dd($request->all());
                $data = DB::select("select bb.id_pesanan,usr.name,nb.nama_barang,bd.jumlah,bb.tanggal_bon,bb.status,
                CASE
                when bb.status = 1 then 'Rikyan'
                when bb.status = 2 then 'Arga'
                ELSE 'Sabar'
                END AS acc
                from tb_bon_barang bb 
                join tb_bon_detail bd on bb.id_pesanan = bd.bon_barang_id 
                join tb_nama_barang nb on bd.barang_id = nb.id
                join users usr on bb.pegawai_id = usr.id
                where usr.id = '$request->petugas'
                order by bb.status DESC");
                // dd($data);
                $keluartotal = DB::select("select sum(bd.jumlah) as total from tb_bon_barang bb join tb_bon_detail bd on bb.id_pesanan = bon_barang_id where bb.pegawai_id = '$request->petugas'");

                $datas = [
                    'data' => $data,
                    'keluartotal' => $keluartotal
                ];

                $pdf = PDF::loadView('admin.laporankeluar.viewlaporan',$datas);
                return $pdf->download('BarangKeluar.pdf');
            }
            
        }

    }

    function laporanjson(Request $request){
        $data = $request->all();
        return response()->json($data);
    }
}
