<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModelSatuanjenis as jenis;

class Jeniscontroller extends Controller
{
    
    public function index()
    {
        $data = jenis::orderBy('id','DESC')->get();
        return view('admin.jenisbarang.index',compact('data'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = jenis::create([
            'nama_jenisbarang' => $request->namajenis
            // 'status' => '1',
        ]);
        if($data){
            return redirect()->route('jenis.index')->with(['success' => 'Data Berhasil Disimpan']);
        //  return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);   
        }else{
            return redirect()->route('jenis.index')->with(['error' => 'Data Gagal Disimpan']);
        }
    }
    public function show($id)
    {
        //
    }

    public function edit($id,jenis $jenis)
    {
        // dd($jenis);
        $data = jenis::find($id);
        // dd($data);
        return view('admin.jenisbarang.edit',compact('data'));
    }
    public function update(Request $request,jenis $jenis)
    {
        // dd($request->all());
        $update = $jenis->update([
            'nama_jenisbarang' => $request->nama_jenisbarang
        ]); 

        // update
        dd($update);
        if($update){
            return redirect()->route('jenis.index')->with(['success' => 'Data Berhasil Diubah!']);
        }
    }
    public function destroy($id)
    {
        //
    }
}
