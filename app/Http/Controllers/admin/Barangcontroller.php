<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ModelSatuanjenis as jenis;
use Illuminate\Http\Request;
use App\Models\ModelBarang as barang;

class Barangcontroller extends Controller
{
    
    public function index()
    {
        $data = barang::orderBy('id','DESC')->get();
        $datajenis = jenis::orderBy('nama_jenisbarang','ASC')->get();
        return view('admin.barang.index',compact('data','datajenis'));
        
    }

    
    public function create()
    {
    }

    
    public function store(Request $request)
    {
        $this->validate($request,[
            'namabarang'=>'required|min:3',
            // 'jenisbarang_id'=>'required|min:3',
            'jumlah'=>'required',
            'harga'=>'required',
        ]);
    //insert
    $data = barang::create([
            'nama_barang' => $request->namabarang,
            'jenis_barang_id' => $request->jenisbarang_id,
            'jumlah' => $request->jumlah,
            'harga' => $request->harga
        ]);

    // dd($data);
        if($data){
            return redirect()->route('barang.index')->with(['success' => 'Data Berhasil Disimpan']);
        //  return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);   
        }else{
            return redirect()->route('barang.index')->with(['error' => 'Data Gagal Disimpan']);
        }


    }

    
    public function show($id)
    {
        //
    }

    
    public function edit(barang $barang)
    {
        // dd($barang);
        $datajenis = jenis::orderBy('nama_jenisbarang','ASC')->get();
        // dd($datajenis);
        return view('admin.barang.edit',compact('barang','datajenis'));
    }

    
    public function update(Request $request, barang $barang)//update barang
    {
        // dd($request->all());
        $update = $barang->update([
            'nama_barang' => $request->nama_barang,
            'jenis_barang_id'=>$request->jenis_barang,
            'jumlah' => $request->jumlah,
            'harga'=> $request->harga
        ]); 
        if($update){
            return redirect()->route('barang.index')->with(['success' => 'Data Berhasil Diubah!']);
        }
    }

    
    public function destroy($id)
    {
        //
    }
}
