<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modelsatuanjenis as jenis;
use App\Models\Modelpemasok as penyedia;
use App\Models\Modeltransaksimasuk as transaksimasuk;
use App\Models\Modeltransaksimasukdetail as transaksimasuk_detail;
use App\Models\ModelBarang as barang;
use DB;
use DataTables;

class Laporanmasukcontroller extends Controller
{
    function index(){
        $petugas = DB::select('select user.id,user.name from users user join model_has_roles hr on user.id = hr.model_id order by hr.model_id DESC');
        return view('admin.laporanmasuk.index',compact('petugas'));
    }

    function laporanmasukpost(Request $request){
        $databarang_masuk = DB::select("SELECT nama_barang,j.nama_jenisbarang, c.jumlah_masuk,a.jumlah as stokterkini from tb_nama_barang a
        left join 
        (select barang_id, sum(a.jumlah) as jumlah_masuk from tb_transaksi_barang_masuk_detail a
        group by barang_id ) as c 
        on a.id = c.barang_id
        join tb_jenis_barang j
        on j.id = a.jenis_barang_id");

        $collect = collect($databarang_masuk);
         /*barang kondisi stok terkini pada tabel nama barang */
        $barang = barang::all();
        return view('admin.laporanmasuk.viewstokbarang',compact('collect'));

    
        
    




        if(!isset($request->radio)){
            dd("variabel belum diset");
        }else{
            if($request->radio == "semuadata"){

            }elseif($request->radio == "tanggal"){

                dd($request->all());

            }else{

            }

        }
    
    }

    public function laporanstok(Request $request){

        $databarang_masuk = DB::select("select c.nama_barang as namabarang, barang_id as idbarang, sum(a.jumlah) as jumlah_masuk from tb_transaksi_barang_masuk_detail a
        join tb_transaksi_barang_masuk b on a.transaksi_barang_masuk_id = b.id
        join tb_nama_barang c on a.barang_id = c.id
        GROUP by barang_id
        ");
        dd($databarang_masuk);

    }












    //datatable
    public function filtercari(Request $request){
        $dtarr = [];
        $param_one =  $request->jenis; //0
        $param_two = $request->penyedia; //0
        $date = $request->tanggal_filter; //null

        if($param_one == '0' && $param_two == '0' && $date == '' ){
            dd("kosong");
            //tampilkan semua
        }elseif($param_one == '0' or $param_two !== '0' or $date == '' ){ // like
            dd("or");
        }else{

        }
        //if kondisi kosong maka tampilkan semua 
        //table tb_transaksi_barang_masuk, join ke tb_pemasok, join tb_transaksi_barang_masuk_detail,
        $datasemua = $users = DB::table('tb_transaksi_barang_masuk')
                            ->join('tb_pemasok','tb_pemasok.id','=','tb_transaksi_barang_masuk.pemasok_id')
                            ->join('tb_transaksi_barang_masuk_detail','tb_transaksi_barang_masuk.id','=','tb_transaksi_barang_masuk_detail.transaksi_barang_masuk_id')
                            ->join('tb_nama_barang','tb_nama_barang.id','=','tb_transaksi_barang_masuk_detail.barang_id')
                            ->select('tb_transaksi_barang_masuk.*','tb_pemasok.nama_pemasok','tb_transaksi_barang_masuk_detail.*','tb_nama_barang.nama_barang')
                            ->orderBy('tb_transaksi_barang_masuk.tanggal','DESC');
                            // ->get();
        return DataTables::of($datasemua)
                            ->addColumn('aksi',function($row){
                                $btn = '<a href="javascript:void(0)" class="btn btn-primary btn-sm">View</a> <a href="javascript:void(0)" class="btn btn-primary btn-sm">Detail</a>';
                                return $btn;
                            })
                            ->toJson();         
        // 1. semuajenis barang dan semua penyedia dan tanggal
        dd($datasemua);
        // 2. barang dipilih dan semau penyedia dan tanggal

        // 3. barang dipilih dan penyedia dipilih dan tanggal
        //if parameter 
        $jenisbarang = jenis::orderBy('nama_jenisbarang','ASC')->get();
        $penyedia = penyedia::orderBy('nama_pemasok','ASC')->get();
        return view('admin.laporanmasuk.index',compact('jenisbarang','penyedia','datasemua'));
    }
    //filtet
    public function filter($data){
        return $data;
    }
}
