<?php

namespace App\Http\Controllers\admin;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ModelBarang as barang;
use App\Models\Modelpemasok as penyedia;
use App\Models\ModelSatuanjenis as jenis;
use App\Models\Modeltransaksimasuk as transaksimasuk;
use App\Models\Modeltransaksimasukdetail as transaksidetail;

class Transaksimasukcontroller extends Controller
{

    public function __construct(){
        if(Auth::check()) {
                // return 'no';
                dd("session habis construct");
            }
    }

    public function index(){
        // untuk select 2
        // dd("test");
        $penyedia = penyedia::orderBy('nama_pemasok','ASC')->get();
        $barang = barang::orderBy('nama_barang','ASC')->get();
        return view('admin.transaksimasuk.index',compact('barang','penyedia'));
    }

    public function store(Request $request){
        
        //insert tabel header
        $transaksimasuk = transaksimasuk::create([
                    'pemasok_id' => $request->penyedia,
                    'tanggal' => \Carbon\Carbon::parse($request->tanggal)->format("Y-m-d"),
                    'keterangan' => 'demo',
                    'nomerpo' => $request->nomerpo     
        ]);

        //insert table detail 
        $count = count($request->barang_id);
        for($i=0;$i<$count;$i++){
            $transaksidetail = transaksidetail::create([
                'transaksi_barang_masuk_id' => $transaksimasuk->id,
                'barang_id' => $request->barang_id[$i],
                'jumlah' => $request->jumlah[$i],
                'satuan_id' => 1,
                'status_id' => 0,
            ]); 
            //update stok
            $jumlahbaranglama = barang::where('id',$request->barang_id[$i])->first();
            $stoklama = $jumlahbaranglama->jumlah;
            $jumlahbaranglama->update([
                'jumlah' => $stoklama + $request->jumlah[$i]
            ]);  
        }
        //redirect data berhasil di insert
        return redirect()->route('transaksimasuk')->with(['success' => 'Transaksimasuk Berhasil Disimpan']);
        
    }

    public function viewtransaksimasuk(){
        return view('admin.laporanmasuk.viewtransmasuk');
    }
}
