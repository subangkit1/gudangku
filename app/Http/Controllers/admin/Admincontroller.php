<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ModelBarang as barang;
use App\Models\Modelbonbarang as bon;
use \Carbon\Carbon;
use DB;
use App\Models\Rolehasuser as ru;
use App\Models\User as user;

use Yajra\DataTables\Facades\DataTables;

class Admincontroller extends Controller
{
    // buat construct_
    public function index(){
        $barang = barang::paginate(10);
        $test = bon::all();
        $bonbarang = bon::whereDate('tanggal_bon',Carbon::today()->toDateString())->get();
        $notvalidateall = bon::where('status',0)->count();
        return view('admin.index',compact('barang','bonbarang','notvalidateall'));
    }

    public function validasi(){
        $datas = bon::orderBy('tanggal_bon','DESC')->get();
        return view('admin.validasi.index',compact('datas'));
    }

    public function validasi_admin($id){
        $data =  bon::where('id_pesanan',$id)->first();
        $data->status = 2 ;
        $data->save();
        if($data){
            return response()->json([
                'message' => 'Data Sukses di Update',
                'status_code' => 200
            ]);
        }else{
            dd("gagal update");
        }
    } 

    public function datas(){
        // return DataTables::of(bon::query())->toJson();

        // conth memakao rows
        $service = DB::table('users')
                    ->select('name', 'email as user_email')
                    ->get();
        dd($service);
    }
    public function admin_roleuser(){
        $datarole = ru::orderBy('role_id','ASC')->get();
        return view('admin.role.index',compact('datarole'));
    }

    public function admin_user(){
        // memakai rightjoin ke tabel role
        $datauser = DB::table('users')
                    ->leftJoin('model_has_roles','users.id','=','model_has_roles.model_id')
                    ->orderBy('id','DESC')
                    ->get();
        // dd($datauser);
        return view('admin.user.index',compact('datauser'));
    }
    public function admin_user_id($id){
        $data = user::where('id',$id)->get();
        return response()->json($data);
    }

    public function admin_user_assign(Request $request){
        // assign ke tabel model_has_roles
        $roleuser = ru::create([
            'role_id' => $request->role_id,
            'model_type' => 'App\Models\User',
            'model_id' => $request->model_id
        ]);

        if($roleuser){
            return redirect()->route('role.admin_user')->with(['success' => 'Data Berhasil Disimpan']);
        //  return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);   
        }else{
            return redirect()->route('role.admin_user')->with(['error' => 'Data Gagal Disimpan']);
        }
        


    }
}
