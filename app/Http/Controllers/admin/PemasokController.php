<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Modelpemasok as pemasok;

class PemasokController extends Controller
{
    
    public function index()
    {
        $data = pemasok::orderBy('id','DESC')->get();
        // dd($data);
        return view('admin.pemasok.index',compact('data'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $data = pemasok::create([
            'nama_pemasok' => $request->namapemasok,
            'no_telp' => $request->telp,
            'alamat_pemasok' => $request->alamat,
            // 'harga' => $request->harga
        ]);

        if($data){
            return redirect()->route('pemasok.index')->with(['success' => 'Data Berhasil Disimpan']);
        }else{
            return redirect()->route('pemasok.index')->with(['error' => 'Data Gagal Disimpan']);
        }

    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(pemasok $pemasok)
    {
        return view('admin.pemasok.edit',compact('pemasok'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,pemasok $pemasok)
    {
        
        $update = $pemasok->update([
            'nama_pemasok' => $request->nama_pemasok,
            'no_telp'=>$request->no_telp,
            'alamat_pemasok' => $request->alamat_pemasok,
        ]); 
        if($update){
            return redirect()->route('pemasok.index')->with(['success' => 'Data Berhasil Diubah!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
