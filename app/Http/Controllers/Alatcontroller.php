<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelalat as Alat;

class Alatcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alats = Alat::all();
        return view('alat.index', compact('alats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_alat' => 'required|string|max:255',
            'jam_alat' => 'required|integer',
            'status' => 'required|string|max:50',
        ]);
    
        Alat::create($request->all());
    
        return redirect()->route('alats.index')
                         ->with('success','Alat berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alat = Alat::findOrFail($id);
        return view('alat.show', compact('alat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alat = Alat::findOrFail($id);
        return view('alat.edit', compact('alat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_alat' => 'required|string|max:255',
            'jam_alat' => 'required|integer',
            'status' => 'required|string|max:50',
        ]);
    
        $alat = Alat::findOrFail($id);
        $alat->update($request->all());
    
        return redirect()->route('alats.index')
                         ->with('success','Alat berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alat = Alat::findOrFail($id);

        // Melakukan pengecekan jika alat berhasil dihapus
        if ($alat->delete()) {
            return redirect()->route('alats.index')
                            ->with('success', 'Alat berhasil dihapus.');
        } else {
            return redirect()->route('alats.index')
                            ->with('error', 'Gagal menghapus alat.');
        }
    }
}
