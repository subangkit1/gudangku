<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Rolehasuser as role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $id = Auth::user()->id; //user id satu hanya bisa 1 login 
        $role = role::where('model_id',$id)->first();

        
        if($role->role_id === 1){ //admin 1
            // dd("admin");
            return redirect()->route('admin.page');
            // return view('admin.index');
        }else{//user 2
            // dd("user");
            return redirect()->route('user.page');
            // return view('user.index');
        }

        if($role->role_id === 4){
            return redirect()->route('rumahsakit');
        }
        // $id = Auth::user()->id; //user id satu hanya bisa 1 login 
        return view('home');
    }
}
