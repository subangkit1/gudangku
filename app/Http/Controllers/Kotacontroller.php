<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kota;
use App\Provinsi;


class Kotacontroller extends Controller
{
    
    public function index()
    {
        $kotas = Kota::all();
        return view('kotas.index', compact('kotas'));
    }

    
    public function create()
    {
        $provinsis = Provinsi::all();
        return view('kotas.create', compact('provinsis'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'prov_id' => 'required|exists:tb_provinsi,id',
            'nama_kota' => 'required|string|max:255',
            'status' => 'required|string|max:50',
        ]);

        Kota::create($request->all());

        return redirect()->route('kotas.index')
                         ->with('success','Kota berhasil ditambahkan.');
    }

    
    public function show($id)
    {
        $kota = Kota::findOrFail($id);
        return view('kotas.show', compact('kota'));
    }

   
    public function edit($id)
    {
        $kota = Kota::findOrFail($id);
        $provinsis = Provinsi::all();
        return view('kotas.edit', compact('kota', 'provinsis'));
    }

  
    public function update(Request $request, $id)
    {
        $request->validate([
            'prov_id' => 'required|exists:tb_provinsi,id',
            'nama_kota' => 'required|string|max:255',
            'status' => 'required|string|max:50',
        ]);

        $kota = Kota::findOrFail($id);
        $kota->update($request->all());

        return redirect()->route('kotas.index')
                         ->with('success','Kota berhasil diperbarui.');
    }

  
    public function destroy($id)
    {
        $kota = Kota::findOrFail($id);
        $kota->delete();

        return redirect()->route('kotas.index')
                         ->with('success','Kota berhasil dihapus.');
    }
}
