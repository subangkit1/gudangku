<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategorialat;

class Kategorialatcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategorialats = Kategorialat::all();
        return view('kategorialats.index', compact('kategorialats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategorialats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kodekategori' => 'required|unique:kategorialats',
            'nama' => 'required|string|max:255',
            'status' => 'required|string|max:50',
        ]);

        Kategorialat::create($request->all());

        return redirect()->route('kategorialats.index')
                         ->with('success','Kategori alat berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategorialat = Kategorialat::findOrFail($id);
        return view('kategorialats.show', compact('kategorialat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategorialat = Kategorialat::findOrFail($id);
        return view('kategorialats.edit', compact('kategorialat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kodekategori' => 'required',
            'nama' => 'required|string|max:255',
            'status' => 'required|string|max:50',
        ]);

        $kategorialat = Kategorialat::findOrFail($id);
        $kategorialat->update($request->all());

        return redirect()->route('kategorialats.index')
                         ->with('success','Kategori alat berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategorialat = Kategorialat::findOrFail($id);
        $kategorialat->delete();

        return redirect()->route('kategorialats.index')
                         ->with('success','Kategori alat berhasil dihapus.');
    }
}
