<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // saya dd disini 
        
        $this->middleware('guest')->except('logout');
    }

     public function authenticated(Request $request, $user)
    {
        // dd($user);


        if($user->role_id == 4){
            return redirect()->route('rumahsakit');
        }

        if ($user->hasRole('admin')) 
        {
            return redirect()->route('admin.page');
        }
        elseif($user->hasRole('user'))
        {
        return redirect()->route('user.page');
        }
        // elseif($user->hasRole('rumahsakit'))
        // {
        // return redirect()->route('rumahsakit');
        // }
        return redirect()->route('user.page');
    }
}
