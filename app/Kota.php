<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'tb_kota';
    protected $primaryKey = 'id';
    protected $fillable = ['id','prov_id','nama_kota','status'];//isi tabel
    public $timestamps = false;

    public function provinsi(){
        return $this->hasOne(Provinsi::class,'id','prov_id');
        // return $this->hasOne(Cutikategorimodel::class,'id','cuti_categories_id');
    }
}
