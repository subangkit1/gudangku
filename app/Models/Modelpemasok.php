<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelpemasok extends Model
{
    use HasFactory;

    protected $table = 'tb_pemasok';
    protected $fillable = ['nama_pemasok','no_telp','alamat_pemasok'];
    protected $primayKey = 'id';
    public $timestamps = false;
    
}
