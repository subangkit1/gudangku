<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelbonbarang extends Model
{
    use HasFactory;

    protected $table = 'tb_bon_barang';//nama tabel
    protected $primaryKey = 'id_pesanan';
    protected $fillable = ['pegawai_id','tanggal_bon','status','keterangan'];//isi tabel
    public $timestamps = false;


    public function pegawai(){
        return $this->hasOne(User::class,'id','pegawai_id');
    }

    public function bondetail(){
        return $this->hasMany(Modelbondetail::class,'bon_barang_id','id_pesanan');
    }

   


}
