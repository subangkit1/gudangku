<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// ambil model didalam folder rs
use App\Models\Rumahsakit\ModelPermohonankalibrasi;

class Modelpermohonanpegawai extends Model
{
    use HasFactory;

    protected $table = 'permohonan_pegawai';//nama tabel
    protected $primaryKey = 'id';
    protected $fillable = ['permohonan_id','pegawai_id','tanggal','status'];
    public $timestamps = false;

    public function getpegawai(){
        return $this->hasOne(Modelpegawaikal::class,'user_id','pegawai_id');
    }

    public function getpegawaiuser(){
        return $this->hasOne(Modelpegawaikal::class,'id','pegawai_id');
    }

    public function getpermohonanrs(){
        return $this->belongsTo(ModelPermohonankalibrasi::class,'id','permohonan_id');
    }


}
