<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class ModelBarang extends Model
{
    use HasFactory;
    protected $table = 'tb_nama_barang';//nama tabel
    protected $primaryKey = 'id';
    protected $fillable = ['nama_barang','jenis_barang_id','jumlah','harga'];//isi tabel
    public $timestamps = false;

  
        public function jenisbarang()
        {
            return $this->hasMany(ModelSatuanjenis::class, 'id', 'jenis_barang_id');
        }


}
