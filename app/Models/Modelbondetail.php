<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelbondetail extends Model
{
    use HasFactory;
    protected $table = 'tb_bon_detail';//nama tabel
    protected $primaryKey = 'bon_id';
    protected $fillable = ['bon_barang_id','barang_id','jumlah','status'];//isi tabel
    public $timestamps = false;


    public function bonbarang(){
        return $this->belongsToMany(Modelbonbarang::class,'id','bon_barang_id');
    }

    public function barang(){
        return $this->belongsTo(ModelBarang::class,'id','barang_id');
    }

    public function tobarang(){
        return $this->hasOne(ModelBarang::class,'id','barang_id');
    }

}
