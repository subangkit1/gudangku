<?php

namespace App\Models\Rumahsakit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modeltransaksiprioritas extends Model
{
    use HasFactory;
    protected $table = 'tb_prioritas_transaksi';
    protected $primaryKey = 'id';
    protected $fillable = ['transaksi_id','prioritas_satu','prioritas_dua','prioritas_tiga','tgl_insert','status'];
    public $timestamps = false;
}
