<?php

namespace App\Models\Rumahsakit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Modelpermohonanpegawai;

class ModelPermohonankalibrasi extends Model
{
    use HasFactory;

    protected $table = 'tb_permohonan';
    protected $primaryKey = 'id';
    protected $fillable = ['nomorsurat_rs','rumahsakit_id','p1','tgl_insert'];
    public $timestamps = false;

    public function getDetail(){
        return $this->hasMany(Modelpermohonandetail::class,'permohonan_id','id')->orderBy('id','DESC');
    }

    public function getPrioritas(){
        return $this->hasOne(Modeltransaksiprioritas::class,'transaksi_id','id');
    }

    public function getRumahsakit(){
        return $this->hasOne(Modelrumahsakit::class,'id','rumahsakit_id');

    }
    public function getpermohonanpegawai(){
        return $this->hasMany(Modelpermohonanpegawai::class,'permohonan_id','id');
    }

    
}
