<?php

// namespace App\Models\rumahsakit;

namespace App\Models\rumahsakit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelpermohonandetail extends Model
{
    use HasFactory;

    protected $table = 'tb_permohonan_detail';
    protected $primaryKey = 'id';
    protected $fillable = ['permohonan_id','alat_id','catatan','qty','harga'];
    public $timestamps = false;

    public function getAlat(){
        return $this->hasOne(\App\Models\Rumahsakit\Modelalat::class,'id','alat_id');
    }
}
