<?php

namespace App\Models\rumahsakit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelrumahsakit extends Model
{
    use HasFactory;
    protected $table = 'tb_rumahsakit';
    protected $primaryKey = 'id';
    // protected $fillable = ['nama_satuan'];
    public $timestamps = false;
}
