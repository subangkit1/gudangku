<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modeltransaksimasukdetail extends Model
{
    use HasFactory;
    protected $table = 'tb_transaksi_barang_masuk_detail';
    protected $fillable = ['transaksi_barang_masuk_id','barang_id','jumlah','satuan_id','status_id'];
    protected $primayKey = 'id';
    public $timestamps = false;

    public function barang(){
        return $this->hasOne(ModelBarang::class,'id','barang_id');
    }

    public function toHeader(){
        return $this->belongsToMany(Modeltransaksimasuk::class,'id','transaksi_barang_masuk_id');
    }

}
