<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modeldivisi extends Model
{
    use HasFactory;

    protected $table = 'tb_divisi';
    protected $fillable = ['nama_divisi','status'];
    protected $primayKey = 'id';
    public $timestamps = false;
}
