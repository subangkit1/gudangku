<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelkeahlianPegawai extends Model
{
    use HasFactory;
    protected $table = 'tb_keahlian_pegawai';
    protected $primaryKey = 'id';

}
