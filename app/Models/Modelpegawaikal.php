<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelpegawaikal extends Model
{
    use HasFactory;

    protected $table = 'tb_pegawai';
    protected $primaryKey = 'id';
    protected $fillable = ['nip','nama','jenis_kelamin','divisi_id','level_id','user_id'];
    public $timestamps = false;
}
