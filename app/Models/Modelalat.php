<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelalat extends Model
{
    use HasFactory;

    protected $table = 'tb_alat';//nama tabel
    protected $primaryKey = 'id';

    public $timestamps = false;


}
