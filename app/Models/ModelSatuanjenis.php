<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelSatuanjenis extends Model
{
    use HasFactory;

    protected $table = 'tb_jenis_barang';
    protected $primaryKey = 'id';
    protected $fillable = ['nama_jenisbarang'];
    public $timestamps = false;

    public function barang(){
        // return $this->hasOne();

    }
}
