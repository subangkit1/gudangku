<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rolehasuser extends Model
{
    use HasFactory;
    protected $table = 'model_has_roles';
    protected $fillable = ['role_id','model_type','model_id'];
    protected $primayKey = 'model_id';
    public $timestamps = false;


    public function ambil_user(){
        return $this->hasOne(user::class, 'id', 'model_id');
    }
}
