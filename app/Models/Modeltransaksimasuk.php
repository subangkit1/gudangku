<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modeltransaksimasuk extends Model
{
    use HasFactory;
    protected $table = 'tb_transaksi_barang_masuk';
    protected $fillable = ['pemasok_id','tanggal','keterangan','nomerpo'];
    protected $primayKey = 'id';
    public $timestamps = false;

    public function pemasok(){
        return $this->hasOne(Modelpemasok::class,'id','pemasok_id');
    }

    public function transaksidetail(){
        return $this->hasMany(Modeltransaksimasukdetail::class,'transaksi_barang_masuk_id',id);
    }

    

}
